/* 
 * Originally written by Alex Wickes and Gil Tabak.
 * Modified by Karanbir Toor
 *
 */

#include "star.h"
#include <vector>
#include "catalog.h"
#include <map>
#include <algorithm>

#pragma once


using namespace std;

typedef unsigned int vote_t;
typedef map<index_t, vote_t> candidate_map;

// After filling the map structure, the data is passed to a vector for easy sorting by number of votes
typedef pair<index_t, vote_t> candidate_pair;
typedef vector<candidate_pair> candidate_vector;

#ifndef IDENTIFIER_H
#define	IDENTIFIER_H 

class identifier {
public:
        void IdentifyImageStars(std::vector<image_star>& ImageStars, Catalog* theCatalog, prior_s* Prior);
private:
        static bool compare_catalog(catalog_pair first, catalog_pair second);
        static bool compare_map( candidate_map::value_type& left, candidate_map::value_type& right);
        static bool compare_vector( candidate_pair& left, candidate_pair& right );
        
        //Adds 1 vote for a catalog star to a particular image stars. Creates element if doesn't exist.
        static inline void add_votes_helper(candidate_map* StarCand, index_t img_star, index_t ctg_star);
                
        static inline void add_votes(candidate_map* StarCand, index_t img_star1, index_t img_star2, index_t ctg_star1, index_t ctg_star2);
        void GetImagePairs(vector<image_star>& ImageStars);
        vector<image_pair> ImagePairs;

        void PrintCurrentVotes(vector<image_star>& ImageStars, candidate_map* StarCandidates);
        void PrintCurrentVotes(vector<image_star>& ImageStars, candidate_vector* StarCandidates);
        void filterByNumberOfVotes(vector<image_star>& ImageStars, candidate_map* from, candidate_vector* to, int minVotes);
        void filterByNumberOfVotes(vector<image_star>& ImageStars, candidate_vector* from, candidate_vector* to, int minVotes);
        void performGeometricVoting(vector<image_star>& ImageStars, candidate_vector* candidates_vec, vector<catalog_star>& CatalogStars);
        int assignHighestVoteToImageStars(vector<image_star>& ImageStars, candidate_vector* candidates_vec);
        void applyStages(vector<image_star>& ImageStars, vector<catalog_star>& CatalogStars, candidate_map* StarCandidates);
        void Print2HighestVotes(vector<image_star>& ImageStars, candidate_vector* StarCandidates);
        void Print2HighestVotes(vector<image_star>& ImageStars, candidate_map* StarCandidates);

        void PurgeImageStars(vector<image_star>& ImageStars, int number);
};

#endif	/* IDENTIFIER_H */