#include "star_map.h"

star_map::star_map(double ra, double dec, double tw, double alpha, double gamma, float garbage,
                int lambda,const char* f)
{
    height = CAMERA_HEIGHT;
    width = CAMERA_WIDTH;
    FocalLength = FOCAL_LENGTH;
    glCatalog.Initialize("catalog.txt", FocalLength);
    
    // Initially set all values to 0.
    data = new unsigned char[2*CAMERA_HEIGHT*CAMERA_WIDTH];
    for (unsigned int i = 0; i<2*CAMERA_WIDTH*CAMERA_HEIGHT; i++) {
        data[i] = 0;
    }
    
    glCatalog.update_all(alpha, gamma, ra, dec, tw);
    
    vector<catalog_star>::size_type sz = glCatalog.Stars.size();         
    unsigned int i;
    
    //DrawCounter = 0;
    //cout << sz << endl;
    cout << "____ Stars drawn at ________" << endl;
    for (i=0; i<sz; i++) {
        //if (abs( glCatalog.Stars[i].X) < CAMERA_WIDTH/2 - 1 && abs(glCatalog.Stars[i].Y) < CAMERA_HEIGHT/2 -1)
        //    DrawCounter++;
        this->Draw_Star( glCatalog.Stars[i].X, glCatalog.Stars[i].Y,SD_DEV, this->abs_mag(glCatalog.Stars[i].app_mag) );
       // if ( !abs(glCatalog.Stars[i].X) > CAMERA_WIDTH/2 - 1 && !abs(glCatalog.Stars[i].Y) > CAMERA_HEIGHT/2 - 1)
                //cout << i << endl;
    }
    cout << "___________________________" << endl;
    
    //cout << "Draw is called on " << DrawCounter << " stars in bounds" << endl;
    
    this->Add_Poisson_Noise(lambda);
    this->printTo_FIT_file(f);
}

star_map::star_map(double ra, double dec, double tw, double alpha, double gamma, float garbage)
{
    height = CAMERA_HEIGHT;
    width = CAMERA_WIDTH;
    FocalLength = FOCAL_LENGTH;
    glCatalog.Initialize("catalog.txt", FocalLength);
    
    // Initially set all values to 0.
    data = new unsigned char[2*CAMERA_HEIGHT*CAMERA_WIDTH];
    for (unsigned int i = 0; i<2*CAMERA_WIDTH*CAMERA_HEIGHT; i++) {
        data[i] = 0;
    }
    
    glCatalog.update_all(alpha, gamma, ra, dec, tw);
    
    vector<catalog_star>::size_type sz = glCatalog.Stars.size();         
    unsigned int i;
    
    //DrawCounter = 0;
    //cout << sz << endl;
    cout << "____ Stars drawn at ________" << endl;
    for (i=0; i<sz; i++) {
        //if (abs( glCatalog.Stars[i].X) < CAMERA_WIDTH/2 - 1 && abs(glCatalog.Stars[i].Y) < CAMERA_HEIGHT/2 -1)
        //    DrawCounter++;
        this->Draw_Star( glCatalog.Stars[i].X, glCatalog.Stars[i].Y,SD_DEV, this->abs_mag(glCatalog.Stars[i].app_mag) );
       // if ( !abs(glCatalog.Stars[i].X) > CAMERA_WIDTH/2 - 1 && !abs(glCatalog.Stars[i].Y) > CAMERA_HEIGHT/2 - 1)
                //cout << i << endl;
    }
    cout << "___________________________" << endl;
    
    //cout << "Draw is called on " << DrawCounter << " stars in bounds" << endl;
    
}

star_map::star_map(const char* f){
    height = CAMERA_HEIGHT;
    width = CAMERA_WIDTH;
    FocalLength = FOCAL_LENGTH;
    glCatalog.Initialize("catalog.txt", FocalLength);
    
    if(f == NULL){
        // Initially set all values to 0.
        data = new unsigned char[2*CAMERA_HEIGHT*CAMERA_WIDTH];
        for (unsigned int i = 0; i<2*CAMERA_WIDTH*CAMERA_HEIGHT; i++) {
                data[i] = 0;
        }
    } else {
    
    unsigned int size;
    char* buffer;
    
    ifstream infile (f,ifstream::binary);
    
    infile.seekg(0,ifstream::end);
    size=infile.tellg();
    infile.seekg(HEADER_SIZE);
    
    buffer = new char[2*CAMERA_HEIGHT*CAMERA_WIDTH];
    
    infile.read (buffer,size);
    infile.close();
    
    data = reinterpret_cast<unsigned char*>(buffer);
    
    //reading the file in switches the order of the two bytes, so we 
    //need to flip them back. fit files use two bytes per pixel
    unsigned char prev = 0;
    unsigned char temp;
    for (unsigned int i = 1; i<2*CAMERA_WIDTH*CAMERA_HEIGHT-1; i++) {
        temp = data[i];
        data[i] = prev;
        prev = temp;
    }
    
    /*for (unsigned int i = 0; i<2*CAMERA_WIDTH*CAMERA_HEIGHT; i++) {
        if ((int)data[i] != 0) {
            cout << (int)data[i] << endl;
        }
    }*/
    }
}

void star_map::printImageStars()
{
    for(int j = 0; j < this->ImageStars.size(); j++ ){
            cout << this->ImageStars[j].r_prime[0] << ", " << this->ImageStars[j].r_prime[1] << ", " << this->ImageStars[j].r_prime[2] << endl;
    }
    
}

void star_map::printGeneratedImageStars()
{
    for(int j = 0; j < this->GeneratedImageStars.size(); j++ ){
            cout << this->GeneratedImageStars[j].r_prime[0] << ", " << this->GeneratedImageStars[j].r_prime[1] << ", " << this->GeneratedImageStars[j].r_prime[2] << endl;
    }
    
}


void star_map::printTo_FIT_file(const char* f)
{
    ofstream file;
    file.open (f);
    
    // Header is 469 bytes long.
    file << "SIMPLE  =     T";
    for (int i = 0; i<80-15; i++) {
        file << " ";
    }
    file << "BITPIX  =    16";
    for (int i = 0; i<80-15; i++) {
        file << " ";
    }
    file << "NAXIS   =     2";
    for (int i = 0; i<80-15; i++) {
        file << " ";
    }
    file << "NAXIS1  =   1280";
    for (int i = 0; i<80-16; i++) {
        file << " ";
    }
    file << "NAXIS2  =   960";
    for (int i = 0; i<80-15; i++) {
        file << " ";
    }
    file << "END";
    for (int i = 0; i<80-15; i++) {
        file << " ";
    }
    
    // Now I add the padding because the header must be 2880 bytes long. 
    unsigned char c = 0;
    for (int i = 0; i<2880-469; i++) {
        file << c;
    }
    
    for (int i=0; i<2*CAMERA_WIDTH*CAMERA_HEIGHT; i++) {
        file << data[i];
    }    
    
    c = 0;
    for (int i=0; i< 2880 - ((2*CAMERA_WIDTH*CAMERA_HEIGHT) % 2880) ; i++) {
        file << c;
    }
    
    file.close();
}

void star_map::print_HISTOGRAM()
{
    
    for (int i=0; i<2*CAMERA_WIDTH*CAMERA_HEIGHT; i++) {
        if (data[i] != 0) {
            cout << "Non-zero pixel!\n";
        }
    } 
}

//----------------------------------------------------------------------
// The next three function are helper functions so we can access our 
// one dimensional data array as a more natural two dimensional counterpart. 
//----------------------------------------------------------------------
// returns index value in the array for the first char of a pixel located at (i,j)
int star_map::index(int i, int j)
{
	return 2*( CAMERA_WIDTH*(j + CAMERA_HEIGHT / 2) + ( i - CAMERA_WIDTH / 2) );
}


int star_map::get(int i, int j)
{
	int ind = index(i,j);
	// If out of bounds, then return -1. Perhaps replace with exception???
	if (ind < 0 || ind >= 2 * CAMERA_WIDTH * CAMERA_HEIGHT || 
                abs(i) > CAMERA_WIDTH/2 - 1 || abs(j) > CAMERA_HEIGHT/2 - 1)
		return -1;

	return (int) ( data[ind] + 256 * data[ind + 1]);		//CHANGE: switched order (seems like fits are read the other way)
}

void star_map::set(int i, int j, int value)
{
	//note 2 * width * height and index are both even, so the condition in the if is sufficient to prevent out-of-bounds.
	//if (-CAMERA_WIDTH / 2 <= i && i < CAMERA_WIDTH / 2 && -CAMERA_HEIGHT / 2 <= j && j < CAMERA_HEIGHT / 2 )
	{
		//better out of bounds condition than the one commented out above.
        int ind = index(i,j);
		if (ind < 0 || ind >= 2 * CAMERA_WIDTH * CAMERA_HEIGHT || 
                abs(i) > CAMERA_WIDTH/2 - 1 || abs(j) > CAMERA_HEIGHT/2 - 1)
			return;
		
		//was 2^16-1. I changed this to 2^8-1 since these are the only values we will need.
		if (value > 4095 )
			value = 4095;
		else if (value < 0)
			value = 0;
        
		data[ind] =  value % 256;				//CHANGE: switched order
		data[ind + 1] =  value / 256; 				//CHANGE: switched order
	}
}

void star_map::add(int i, int j, int value)
{
	int current = this->get(i,j);
	this->set(i, j, current+value);
}
// ========================================================================
// ========================================================================

// This is the Gauss error function, used to make approximations
double erf(double x)
{
    // constants
    double a1 =  0.254829592;
    double a2 = -0.284496736;
    double a3 =  1.421413741;
    double a4 = -1.453152027;
    double a5 =  1.061405429;
    double p  =  0.3275911;
    
    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x);
    
    // A&S formula 7.1.26
    double t = 1.0/(1.0 + p*x);
    double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);
    
    return sign*y;
}

double psn(int lambda)
{
    double L = exp(- ((double)lambda) );
    int k = 0;
    double p = 1;
    while(p > L)
    {
        k++;
        double uniform_rand = (double)rand()/RAND_MAX;
        p = p * uniform_rand;
        //cout << uniform_rand << endl;
    }
    return k - 1 ;
}

// Draws a star at coordinate X,Y with standard deviation stDev.
// mag refers to the total area under the Gaussian surface of the star.
void star_map::Draw_Star(double X, double Y, double stDev, double mag)
{
    //debug
    //cout << "X:  " << X << "   Y:  " << Y << endl;
    if ( abs(X) > CAMERA_WIDTH/2 - 1 || abs(Y) > CAMERA_HEIGHT/2 - 1) {
        return;
    }
    //DrawCounter++;

    
    cout << "X: " << X << " Y: " << Y << endl;
    // TODO: the following is used to test the detector by removing its function, should be taken out
    // also as you can see in the main file this image stars vector is passed on the clean star-map
    image_star star;
    star.centroid_x = X;
    star.centroid_y = Y;
    star.error = 0;
    star.radius = 6 * (int) stDev;
    star.identity = INDEX_INVALID;
    star.error = 3.0*ERROR_FUNCTION( atan( star.radius / 4293.0 ) );
    GetSphericalFromImage( star.centroid_x, star.centroid_y, 4293.0, star.r_prime );
    this->GeneratedImageStars.push_back(star);
    //_____________________________________
    
    
    //____________________________________
	//don't bother drawing farther than 6 stDevs
	int radius = 6 * (int) stDev;
	
	//scan a square around where we want the star
	//old version
	/*for (int j = Y - radius; j <= Y + radius; j++)
	{
		// row_mag is the are under the curve for a given value of j.
		// Note we divide by 2 since erf(infinity) - erf(-infinity) = 2
		double row_mag = 0.5 * mag * abs( erf( (Y - (double)j - 0.5) / stDev ) - erf( (Y - (double)j + 0.5) / stDev ) );
		//cout << row_mag << endl;
		for (int i = X - radius; i <= X + radius; i++)
		{
			// add values to individual pixels with specific (i,j) coordinates
            if ( abs(i) > CAMERA_WIDTH/2 - 1 || abs(j) > CAMERA_HEIGHT/2 - 1) {
                return;
            }
			this->add(i,j, row_mag * 0.5 * abs( erf( (X - (double)i - 0.5) / stDev ) - erf( (X - (double)i + 0.5) / stDev ) ) );
		}
	}*/

	//scan a square around where we want the star
	//new version. Does not use the conditions since the issue was fixed in the setter.
	//Other fix: Will now draw the star centered at the Euclidean coordinates with NO shift
	// e.g. if X = 0.0, Y = 0.0, the Gaussian will be symmetric about 0 in both X and Y.
	// Please note: the data array still stores values as shifted by 0.5, since it stores the X and Y coordinates as integer values.
	for (int j = (int) (Y - radius); j <= (int) (Y + radius); j++)
	{
		// row_mag is the area under the curve for a given value of j.
		// Note we divide by 2 since erf(infinity) - erf(-infinity) = 2. i.e. normalize the Gaussian.
		double row_mag = 0.5 * mag * abs( erf( ((Y - (double)j) - 1.0 ) / stDev ) - erf( ((Y - (double)j) ) / stDev ) );

		for (int i = (int) (X - radius); i <= (int) (X + radius); i++)
		{
			// add values to individual pixels with specific (i,j) coordinates
			//values.add(i,j, row_mag * 0.5 * abs( erf( ((X - (double)i) - 1.0) / stDev ) - erf( ((X - (double)i) ) / stDev ) ) );
            
            //values.add(...) did not compile so I tried this
            this->add(i,j, row_mag * 0.5 * abs( erf( ((X - (double)i) - 1.0) / stDev ) - erf( ((X - (double)i) ) / stDev ) ) );
		}
	}
}

//returns a number proportional to the signal of a star based on the star's apparent magnitude
//the proportionality is determined by brightness
double star_map::abs_mag(double rel_mag)
{ 
    //return 100;
	return BRIGHTNESS*pow(0.4, rel_mag);
}

Catalog* star_map::getRefToCatalog(){
    return &glCatalog;
}

void star_map::Add_Poisson_Noise(int lambda)
{
    srand(time(NULL));
    for(int i = -CAMERA_WIDTH / 2; i < CAMERA_WIDTH / 2; i++)
        for(int j = -CAMERA_HEIGHT / 2 + 1; j < CAMERA_HEIGHT / 2 + 1; j++)
        {
            /*int ind = index(i,j);
            if (ind < 0 || ind >= 2 * width * height)
			return;*/
            this->add(i,j,psn(lambda));
        }
}

//places image stars at exactly the locations specified by the catalog 
//within the prior parameters. Used for testing/developing identifier algorithms
void star_map::populateImageStars(prior_s Prior){
    if ( !this->getRefToCatalog()->initialized )
                return;
    
    Catalog* t = this->getRefToCatalog();
    
    double phi; //angle used to figure out if we are close enough to put the image star in ImageStars

    //vector<catalog_star>::iterator it = t->Stars.begin();
    
    //set up an image_star struct.
    //TODO: test effects of different errors, figure out how to solve when error is big,
    //test effects of actual error,
    image_star im;
    im.centroid_x = 0;
    im.centroid_y = 0;
    im.error = 0.0002; //error associated with each star (to be multiplied by ERROR_MULTIPLIER)
                                //using ERROR_MULTIPLIER = 3.0
    im.identity = -1;
    for (int i = 0; i < 3; i++){
        im.r[i] = 0;
        im.r_prime[i] = 0;
    }
    im.radius = 0;
    
    //add the image stars that are close to the prior
    //TODO: rotate stars to the prior's coordinate system, and use the dot product with the x-axis instead of the prior
    //(right now correct answer will yield x-axis as solution)
    for(int j = 0; j < t->Stars.size(); j++ ){
        phi = acos(sin(t->Stars[j].Dec)*sin(Prior.coords.DEC) 
                 + cos(t->Stars[j].Dec)*cos(Prior.coords.DEC)*cos(t->Stars[j].RA-Prior.coords.RA) );
        if (phi < Prior.phi_max ){
            for (int i = 0; i< 3; i++)
                 im.r_prime[i] = t->Stars[j].r[i];
            //cout << im.r_prime[0] << ", " << im.r_prime[1] << ", " << im.r_prime[2] << endl;
            //cout << "adding star with ID " << j <<endl;
            this->GeneratedImageStars.push_back(im);
        }
    }
    cout << "-------------------------" << endl;
    
    return;    
}

inline double star_map::wrapAngle( double angle )
{
    double twoPi = 2.0 * PI;
    return angle - twoPi * floor( angle / twoPi );
}

void star_map::printAlgorithmData(){
    
    //cout << "Boresight Output\n" << "\t" << BoresightOut.RA << endl << "\t" << BoresightOut.DEC << endl << endl;
    cout << "Coordinates Output\n" << "\t" << wrapAngle(CoordOut.RA) << endl << "\t" << wrapAngle(CoordOut.DEC) << endl << endl;
    
    /*cout << "Rotation Matrix\n";
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            cout << "\t" << RMat[i][j]; 
        }
        cout << endl;
    }
        */  
    /*cout << endl << "Quaternions\n";
    for(int n=0; n<4; n++)
        cout << "\t" << RQuat[n] << endl;
        */
    
}
