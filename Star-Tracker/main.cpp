#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <time.h>
#include <fstream>
#include <math.h>

//#include <stdint.h>

#include "star_map.h"
#include "identifier.h"
#include "static_algorithm.h"

//#define M_PI			3.14159265358979323846264338327950288 (defined in star_map.h)

using namespace std;

//RA and DEC are input for prior
double RA = 0.7;
double DEC = 0.3;
//set up prior to test the identifier in this position
 prior_s p = { {RA, DEC}, 0.3, 0};

// round up and down were used to see how senstive the identifier was to changes in imagestars' coordinates
void roundDown(vector<image_star>& ImageStars){
    std::vector<int>::size_type sz = ImageStars.size();
    
    for(unsigned int i=0;i<sz;i++){
        ImageStars[i].centroid_x = floor(ImageStars[i].centroid_x);
        ImageStars[i].centroid_y = floor(ImageStars[i].centroid_y);
    }
}
void roundUp(vector<image_star>& ImageStars){
    std::vector<int>::size_type sz = ImageStars.size();
    
    for(unsigned int i=0;i<sz;i++){
        ImageStars[i].centroid_x = ceil(ImageStars[i].centroid_x);
        ImageStars[i].centroid_y = ceil(ImageStars[i].centroid_y);
    }
}

// this function lets us know if there is any corruption going on between read/write streams
int compareSMaps(unsigned char* d1, unsigned char* d2){
    for(unsigned int i = 0; i < 960*1280; i++)
        if(d1[i] != d2[i]){
            cout << "FATAL ERROR: THE TWO MAPS ARE DIFFER IN DATA STREAM" << endl;
            return -1;
        }
    return 0;
}

// this function can be used to test the effectiveness of the detector, 
// pass in image-stars from the generated star-map and image-stars from the
// clean star-map, choose a tolerance
// and it will return the number of stars that don't match each other
int compareImageStarCoordinates(vector<image_star>& i1, vector<image_star>& i2, double tolerance){
    std::vector<int>::size_type sz1 = i1.size();
    std::vector<int>::size_type sz2 = i2.size();
    int retVal = 0;
    
    if(sz2 > sz1)
        return compareImageStarCoordinates(i2,i1,tolerance);
    
    for(unsigned int i=0;i<sz1;i++){
        //retVal++;
        for(unsigned int j=0;j<sz2;j++){
            if( abs(i1[i].centroid_x - i2[j].centroid_x) < tolerance 
                && abs(i1[i].centroid_y - i2[j].centroid_y) < tolerance )
                        retVal++;
        }
    }
    
    return retVal;
}

unsigned char* readFileIntoBuffer() {
    std::ifstream is("raw", std::ifstream::binary);
    if (is) {
        // get length of file:
        is.seekg(0, is.end);
        int length = is.tellg();
        is.seekg(0, is.beg);

        char * buffer = new char [length];

        std::cout << "Reading " << length << " characters... ";
        // read data as a block:
        is.read(buffer, length);

        if (is)
            std::cout << "all characters read successfully.";
        else
            std::cout << "error: only " << is.gcount() << " could be read";
        is.close();
        
        // ...buffer contains the entire file...



        return (unsigned char*) buffer;
    }
}



int main(int argc, char* argv[]){
    // the generator is the code inside the star-map that generates the star-map
    // using coordinates ra and dec.
    
    //first one below makes new image and uses it for algorithm, second line opens image.
    star_map*s = new star_map(RA,DEC,0,M_PI/2,0,0,100,"black.fit");
    //star_map* s = new star_map("black.fit");
    
    
    //READ RAW FILE
    /*star_map* s = new star_map(NULL);
    s->data = readFileIntoBuffer();
    s->printTo_FIT_file("black.fit");*/

        
    static_algorithm* SA = new static_algorithm();
    
    SA->runAlgorithm(s);
    
    s->printAlgorithmData();
    return 0;
}
