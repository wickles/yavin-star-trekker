/* identifier.cpp
 * Alexander Wickes and Gil Tabak
 * September 20, 2010
 */

#include <algorithm>    // std::sort
#include "identifier.h"
//modify timer for windows
#include "time.h" 
#include <iostream>
#include <stdio.h>

using namespace std;

//original version used 5 for NUM_CANDIDATES. 
#define NUM_TO_USE              20 //number of image stars to use
#define NUM_CANDIDATES          20
#define NUM_PARTIAL_SORT        20


const int OK_VOTES_1 = 5; //number of votes required by an image star to keep a catalog star
const int OK_VOTES_2 = 5;


bool identifier::compare_catalog(catalog_pair first, catalog_pair second){
	return (first.distance < second.distance);
}

/*	Map to keep track of votes for candidates stars for a given image star. Key is the index of the candidate catalog star,
	value is number of votes for the candidate star. Candidates not found in map are added with 1 vote, those found have their
	votes incremented by 1 */


bool identifier::compare_map( candidate_map::value_type& left, candidate_map::value_type& right){
	return (left.second > right.second);
}

bool identifier::compare_vector( candidate_pair& left, candidate_pair& right ){
	return (left.second > right.second);
}

// Add a vote for a specific catalog star to a specific image star's map
inline void identifier::add_votes_helper(candidate_map* StarCand, index_t img_star, index_t ctg_star){
	candidate_map::iterator it = StarCand[img_star].lower_bound(ctg_star);
	if ( it != StarCand[img_star].end() && it->first == ctg_star )
		it->second += 1;
	else
		StarCand[img_star].insert( it, candidate_map::value_type(ctg_star, 1) );
}


// Add votes for both catalog stars to both image stars' maps
inline void identifier::add_votes(candidate_map* StarCand, index_t img_star1, index_t img_star2, index_t ctg_star1, index_t ctg_star2){
	add_votes_helper(StarCand, img_star1, ctg_star1);
	add_votes_helper(StarCand, img_star1, ctg_star2);
	add_votes_helper(StarCand, img_star2, ctg_star1);
	add_votes_helper(StarCand, img_star2, ctg_star2);
}

// Global vector of image pairs, to avoid allocating memory repeatedly
//vector<image_pair> ImagePairs;

void identifier::GetImagePairs(vector<image_star>& ImageStars){
	ImagePairs.clear();

	//vector<image_pair> ImagePairs;
	ImagePairs.reserve( ImageStars.size() * (ImageStars.size()-1) / 2 );


	int i, j;
	// Iterate over all distinct pairs of image stars
	for ( i = 0; i < ImageStars.size()-1; i++ )
	{
		for ( j = i+1; j < ImageStars.size(); j++ )
		{
			sfloat ang_dist = acos( dot_product(ImageStars[i].r_prime, ImageStars[j].r_prime) );
			// Error is sum of errors for both stars
			sfloat error = ImageStars[i].error + ImageStars[j].error;
			//error *= .5f;

                        //cout << "ang_dist\t" << ang_dist << endl;
			//cout << "error\t" << error << endl;
                        image_pair pair = { i, j, cos(ang_dist + error), cos(ang_dist - error) };
			ImagePairs.push_back(pair);
		}
	}

	//return ImagePairs;
}

//This method is used to cut the number of image stars, in case there are too
//many. This keeps the run-time of the algorithm small
void identifier::PurgeImageStars(vector<image_star>& ImageStars, int number)
{
    //if the number of ImageStars is less then or equal to the given number, do nothing
    if (ImageStars.size() <= number)
        return;
    //sort the image stars by error
    std::sort (ImageStars.begin(), ImageStars.end() );
   
    ImageStars.erase (ImageStars.begin()+number,ImageStars.end() );
    
    vector<image_star>::iterator it;
    for ( it = ImageStars.begin(); it != ImageStars.end(); it++ ){
        printf(" error(%04f) \n", it->error);
    }
}
///////////////////////////////////////////////////////////////////////////////////
        //The following section prints out the image stars with their remaining
        //catalog stars, including the votes for each. For maps.
void identifier::PrintCurrentVotes(vector<image_star>& ImageStars, candidate_map* StarCandidates){
    for (int j = 0; j < ImageStars.size(); j++ ){
		printf("Image Star %d\n", j);
		candidate_map::iterator it;
		for ( it = StarCandidates[j].begin(); it != StarCandidates[j].end(); it++ ){
			printf("	1) CatalogStars ID: %d | Votes: %d\n", it->first, it->second);
		}
	}
}
        //The following section prints out the image stars with their remaining
        //catalog stars, including the votes for each. For vectors.
void identifier::PrintCurrentVotes(vector<image_star>& ImageStars, candidate_vector* StarCandidates){
    for (int j = 0; j < ImageStars.size(); j++ ){
		printf("Image Star %d\n", j);
		candidate_vector::iterator it;
		for ( it = StarCandidates[j].begin(); it != StarCandidates[j].end(); it++ ){
			printf("	%d) CatalogStars ID: %d | Votes: %d\n", j, it->first, it->second);
		}
	}
}
        // The following method prints the two highest voted candidates. 
void identifier::Print2HighestVotes(vector<image_star>& ImageStars, candidate_vector* StarCandidates){
    for (int j = 0; j < ImageStars.size(); j++ ){
		printf("Image Star %d\n", j);
		candidate_vector::iterator it;
                int highest_vote = 0;
                int second_highest_vote = 0;
                int h_index = 0;  // used to track location of highest vote, for finding 2nd highest vote
                int loc = 0;
                        // Find the highest vote.
		for ( it = StarCandidates[j].begin(); loc < StarCandidates[j].size() && it != StarCandidates[j].end(); it++ ){
                    if(it->second > highest_vote){
                        highest_vote = it->second;
                        h_index = loc;
                    }
                    it++;
                    loc++;
		}
                        // Find the second highest vote.
                loc = 0;
                for ( it = StarCandidates[j].begin(); loc < StarCandidates[j].size() && it != StarCandidates[j].end(); it++ ){
                    if(it->second > second_highest_vote && loc != h_index){
                        second_highest_vote = it->second;
                    }
                    it++;
                    loc++;
		}
                printf("\t1st highest vote: %d\n\t2nd highest vote: %d\n",highest_vote,second_highest_vote);
	}
}
void identifier::Print2HighestVotes(vector<image_star>& ImageStars, candidate_map* StarCandidates){
    for (int j = 0; j < ImageStars.size(); j++ ){
		printf("Image Star %d\n", j);
		candidate_map::iterator it;
                int highest_vote = 0;
                int second_highest_vote = 0;
                int h_index = 0;  // used to track location of highest vote, for finding 2nd highest vote
                int loc = 0;
                        // Find the highest vote.
		for ( it = StarCandidates[j].begin(); loc < StarCandidates[j].size() && it != StarCandidates[j].end(); it++ ){
                    if(it->second > highest_vote){
                        highest_vote = it->second;
                        h_index = loc;
                    }
                    it++;
                    loc++;
		}
                        // Find the second highest vote.
                loc = 0;
                for ( it = StarCandidates[j].begin(); loc < StarCandidates[j].size() && it != StarCandidates[j].end(); it++ ){
                    if(it->second > second_highest_vote && loc != h_index){
                        second_highest_vote = it->second;
                    }
                    it++;
                    loc++;
		}
                printf("\t1st highest vote: %d\n\t2nd highest vote: %d\n",highest_vote,second_highest_vote);
	}
}
///////////////////////////////////////////////////////////////////////////////////

        // The following code first restricts possible stars by vote value.
        // This is done by send candidates in the "from" structure to the "to"
        // structure only if it has at least "minVotes".
        // Now also automatically finds the max and second max in "from", 
        // and puts them in the beginning of "to".
        // Polymorphically implemented to handle two possible stages of filter.

void identifier::filterByNumberOfVotes(vector<image_star>& ImageStars, candidate_map* from, candidate_vector* to, int minVotes){

	// iterate over each image star
	for (int i = 0; i < ImageStars.size(); i++ )
	{
		// create a temporary candidate_vector filled with all candidates from the corresponding hash_map
		// note that the OBJECT created by hash_map<index_t,vote_t> is exactly of type candidate_t, that is pair<index_t,vote_t>
		candidate_vector tmp_vec( from[i].begin(), from[i].end() );

		// create an iterator to the NUM_PARTIAL_SORTth element of tmp_vec if number of candidate pairs exceeds NUM_PARTIAL_SORT, else to the last element
		candidate_vector::iterator it_stop = ( tmp_vec.size() > NUM_PARTIAL_SORT ? tmp_vec.begin()+NUM_PARTIAL_SORT : tmp_vec.end() );

		// do a partial (quick) sort of tmp_vec so that the N = [it_mid - tmp_vec.begin()] candidates with the highest votes
		// are at the beginning (in descending order? should be but not sure anymore if that's actually true)
		partial_sort( tmp_vec.begin(), it_stop, tmp_vec.end(), compare_vector );

		// will be used to store the number of votes for the NUM_CANDIDATESth candidate
		int lastCandidateVotes = 1;
		candidate_vector::iterator it;
		// iterates until the NUM_CANDIDATESth candidate, and then until the next candidate with properly fewer votes
		// The second part is so that all stars with the lowest number of votes are included
		for (	it = tmp_vec.begin();
                        it != it_stop && ( int(it - tmp_vec.begin()) < NUM_CANDIDATES || it->second >= lastCandidateVotes );
			it++ )
		{
			// add a copy of the candidate star with no votes to the end of the candidates_vec vector for image star i
			to[i].push_back( candidate_pair(it->first, 0) );
			if ( int(it - tmp_vec.begin() ) == NUM_CANDIDATES-1 )
				// just sets min_votes according to the behavior described immediately before the for loop
				lastCandidateVotes = it->second;
		}
	}
}

void identifier::filterByNumberOfVotes(vector<image_star>& ImageStars, candidate_vector* from, candidate_vector* to, int minVotes){
        for (int j = 0; j < ImageStars.size(); j++ ){
          
            if (ImageStars[j].identity != -1)
                continue;
                
                candidate_vector::iterator it;

                int max = 0;
                //int secondMax = 0;
                //TODO: cut off terms from "to" based on max ?
                
                //now, this part just finds the max. 
                for ( it = from[j].begin(); it != from[j].end(); it++){ 
                    //if(it->second >= minVotes){                               //check for minVotes
                        //if(it->second > secondMax){                           //commented stuff can be used for second highest
                            if(it->second > max){
                                //secondMax = max;
                                max = it->second;
                                //cout << it->second << endl;
                            }
                            /*else{
                                secondMax = it->second;
                                to[j].insert(to[j].begin()+1, candidate_vector::value_type (it->first, 0));
                            }*/
                    //  }
                }

                //filters based on both the given minVotes and max. 
                for ( it = from[j].begin(); it != from[j].end(); it++){
                    if(it->second >= minVotes && it->second >= max*0.3)
                        to[j].push_back( candidate_vector::value_type (it->first, 0) );
                }
	}
}

        // The following code follows a geometric voting algorithm to pairs votes.
        //
void identifier::performGeometricVoting(vector<image_star>& ImageStars, candidate_vector* candidates_vec, vector<catalog_star>& CatalogStars){
        for (int j = 0; j < ImageStars.size()-1; j++ )
	{
		for (int k = j+1; k < ImageStars.size(); k++ )
		{
			candidate_vector::iterator it1, it2;
			for ( it1 = candidates_vec[j].begin(); it1 != candidates_vec[j].end(); it1++ )
			{
				for ( it2 = candidates_vec[k].begin(); it2 != candidates_vec[k].end(); it2++ )
				{
					index_t pair_index = j*ImageStars.size() + k - (j+1)*(j+2)/2;
					sfloat dot = dot_product(CatalogStars[it1->first].r, CatalogStars[it2->first].r);
					sfloat lower = ImagePairs[pair_index].lower
;					sfloat upper = ImagePairs[pair_index].upper;
				if ( dot >= lower && dot <= upper )
					{
						// both stars good, add vote to current map
						it1->second += 1;
						it2->second += 1;
					}
				}
			}
		}
	}
}

        // (The following assigns image stars' identities to the ooted candidate. )
//assigns identities to image stars with no given ID (i.e. ID = -1)
//Condition used is (votes for top candidate) * minProp > (votes for second top candidate)
//otherwise the ID of the image star is unchanged
//returns number of stars identified
int identifier::assignHighestVoteToImageStars(vector<image_star>& ImageStars, candidate_vector* candidates_vec){
    int identified = 0;
    for (int j = 0; j < ImageStars.size(); j++ ){
            if (ImageStars[j].identity != -1){
                identified ++;
                continue;
            }
                candidate_vector::iterator it; 
                it = candidates_vec[j].begin();
                //max = candidates_vec[j].begin();
                //secondMax = candidates_vec[j].begin();

                
                int max = 0;
                int maxID = -1;
                int secondMax = 0;
                int secondMaxID = -1;
                
                while(it != candidates_vec[j].end()){
                        if(it->second > secondMax){
                            if(it->second > max){
                                secondMax = max;
                                secondMaxID = maxID;
                                max = it->second;
                                maxID = it->first;
                            }
                            else{
                                secondMaxID = it->first;
                                secondMax = it->second;
                            }
                        }
                        it++;
                }
                
                cout <<"star " << j << " has top candidates " << endl
                        << maxID << " with " << max << " votes" << endl
                        << secondMaxID << " with " << secondMax << " votes" << endl;

                if(max* 0.8 > secondMax && max - 2 > secondMax){
                    ImageStars[j].identity = maxID;
                    identified ++;
                    cout << "star " << j << " identified as " << maxID << endl;
                }
        }
    return identified;
}

        // This is the main method that is used in the identifier to 
        // run through multiple stages of geometric voting and filtering.
void identifier::applyStages(vector<image_star>& ImageStars, vector<catalog_star>& CatalogStars, candidate_map* StarCandidates){
    candidate_vector* candidates_vec1 = new candidate_vector[ImageStars.size()];
    candidate_vector* candidates_vec2 = new candidate_vector[ImageStars.size()];
    candidate_vector* candidates_vec3 = new candidate_vector[ImageStars.size()];

#ifdef USING_UNIX
  clock_t stage1;
  clock_t stage2;
  clock_t stage3;
  clock_t assignID;
#endif
  
    //Print2HighestVotes(ImageStars, StarCandidates);
    
    cout << "------------------------------------" << endl;
    
#ifdef USING_UNIX
    stage1 = clock();
#endif
    filterByNumberOfVotes(ImageStars,StarCandidates,candidates_vec1,OK_VOTES_1);
    performGeometricVoting(ImageStars,candidates_vec1,CatalogStars);
    
#ifdef USING_UNIX
                stage1 = clock() - stage1;
#endif
    //PrintCurrentVotes(ImageStars, candidates_vec1);
    //Print2HighestVotes(ImageStars, candidates_vec1);
    //assignHighestVoteToImageStars(ImageStars,candidates_vec1);
#ifdef USING_UNIX
                stage2 = clock();
#endif
    filterByNumberOfVotes(ImageStars,candidates_vec1,candidates_vec2,OK_VOTES_2);
    performGeometricVoting(ImageStars,candidates_vec2,CatalogStars);
    
#ifdef USING_UNIX
                stage2 = clock() - stage2;
#endif
    //PrintCurrentVotes(ImageStars, candidates_vec2);
    //Print2HighestVotes(ImageStars, candidates_vec2);
#ifdef USING_UNIX
                stage3 = clock();
#endif
    //after 2 geometric voting iterations, do a possible third if detected fewer than 5 stars
   // if (assignHighestVoteToImageStars(ImageStars,candidates_vec2) < 5){
    filterByNumberOfVotes(ImageStars,candidates_vec2,candidates_vec3,OK_VOTES_2);
    performGeometricVoting(ImageStars,candidates_vec3,CatalogStars);
    
 #ifdef USING_UNIX
                stage3 = clock() - stage3;
                assignID = clock();
#endif
    //PrintCurrentVotes(ImageStars, candidates_vec3);
    //Print2HighestVotes(ImageStars, candidates_vec3);
    assignHighestVoteToImageStars(ImageStars,candidates_vec3);
#ifdef USING_UNIX
                assignID = clock() - assignID;
#endif
    //}
#ifdef USING_UNIX

          cout << "time took for stage 1 " << ((float) stage1) / CLOCKS_PER_SEC << " seconds." << endl;           
          cout << "time took for stage 2 " << ((float) stage2) / CLOCKS_PER_SEC << " seconds." << endl; 
          cout << "time took for stage 3 " << ((float) stage3) / CLOCKS_PER_SEC << " seconds." << endl; 
          cout << "time took for assigning IDs " << ((float) assignID) / CLOCKS_PER_SEC << " seconds." << endl; 
#endif
    
    delete[] candidates_vec1;
    delete[] candidates_vec2;
    delete[] candidates_vec3;
}




void identifier::IdentifyImageStars(vector<image_star>& ImageStars, Catalog* theCatalog, prior_s* Prior)
{
    
#ifdef USING_UNIX    
  clock_t total;
  total = clock();
#endif
        //if using windows
	//Timer timer;
	//timer.StartTimer();
        
	vector<catalog_star>& CatalogStars = theCatalog->Stars;
	vector<catalog_pair>& CatalogPairs = theCatalog->SortedPairs;
        
        //cout << CatalogStars.size() << endl;
        //cout << "---------------> " << CatalogPairs.size() << endl;
        
        PurgeImageStars(ImageStars, NUM_TO_USE);

	GetImagePairs( ImageStars );
        vector<image_star>::iterator i;
	/*for ( i = ImageStars.begin(); i != ImageStars.end(); i++ ){
            cout << "--------------------------" << endl;
                cout << i->centroid_x << endl;
                cout << i->centroid_y << endl;
                cout << "\t" << i->r_prime[0] << endl << "\t" << i->r_prime[1] << endl << "\t" << i->r_prime[2] << endl;
                cout << "\t" << i->r[0] << endl << "\t" << i->r[1] << endl << "\t" << i->r[2] << endl;
                cout << i->error << endl;
                cout << i->radius << endl;
                cout << i->identity << endl;
        }*/
        
        //If there is a prior, it will be used to restrict the catalog
	sfloat rPrior[3];
	sfloat prior_dot;
	if ( Prior != NULL )
	{
		rPrior[0] = cos(Prior->coords.DEC) * cos(Prior->coords.RA);
		rPrior[1] = cos(Prior->coords.DEC) * sin(Prior->coords.RA);
		rPrior[2] = sin(Prior->coords.DEC);
		prior_dot = cos(Prior->phi_max);
	}
        
        //TODO: de-allocate later?
        //initial map, determined by distances only
	candidate_map* StarCandidates = new candidate_map[ImageStars.size()]; 
        
        //takes catalog stars in StarCandidates with sufficiently high votes
        //stars with 0 votes for each, and then add votes based on distances
        //for only the catalog stars present
        //candidate_vector* candidates_vec = new candidate_vector[ImageStars.size()]; 
        
        //second stage. same as before.
	//candidate_map* candidates_vec2 = new candidate_map[ImageStars.size()]; 

	debug_printf("Identifying stars...\n");

        //The next part of the code parses through the image star pairs.
        //For each image star pair, all catalog star pairs of comparable distance
        //are found. Then votes for each of the catalog pairs is added to each
        //of the image stars in each image pair.
                //fill data structure
        
       // clock_t initialVotes;
       // initialVotes = clock();
	vector<image_pair>::iterator it;
	for ( it = ImagePairs.begin(); it != ImagePairs.end(); it++ ){
	
               /* printf("ImagePair(%04d, %04d) | RANGE = [%.15f, %.15f] = cos[%.15f, %.15f]\n", 
					it->star1, it->star2, it->lower, it->upper, acos(it->upper), acos(it->lower) );*/

                catalog_pair tmp = { INDEX_INVALID, INDEX_INVALID, it->lower };

		// do STL binary search for iterator to beginning of range
		vector<catalog_pair>::iterator it_cat = lower_bound( CatalogPairs.begin(), CatalogPairs.end(), tmp, compare_catalog );
                
                //printf( "lower bound, Pair(%d,%d), Dist = %f\n", it_cat->star1, it_cat->star2, it_cat->distance );
                
                //loops through the subset of the catalog pairs that is sufficiently close to the image pair (in distance)
                //cout << "-------->  " << it_cat->distance << endl;
                //cout << "-------->  " << it->upper << endl;
		for ( ; it_cat != CatalogPairs.end() && it_cat->distance  <= it->upper; it_cat++ ){
                        //If there is a prior, exclude catalog pairs which consist of at least one star outside the allowed range
			if ( Prior != NULL && (	dot_product( rPrior, CatalogStars[it_cat->star1].r) < prior_dot ||
						dot_product( rPrior, CatalogStars[it_cat->star2].r) < prior_dot ) )
				continue;
			/*printf("	Adding votes: ImagePair(%03d, %03d), CatalogPair(%04d, %04d) | Distance: %.15f\n",
					it->star1, it->star2, it_cat->star1, it_cat->star2, it_cat->distance);*/

			// for each CatalogStars pair in range, add vote for each CatalogStars star to each image star
			add_votes(StarCandidates, it->star1, it->star2, it_cat->star1, it_cat->star2); // Commenting this line fixes errors
		}
	}
        
       //   initialVotes = clock() - initialVotes;
        //  cout << "time took for setting up initial votes is " << ((float) initialVotes) / CLOCKS_PER_SEC << " seconds." << endl; 
        
        applyStages(ImageStars,CatalogStars,StarCandidates);
        
        cout << "done applying stages!!" << endl;
      //    total = clock() - total;
       //   cout << "time took for stages is " << ((float) total) / CLOCKS_PER_SEC << " seconds." << endl; 
               
        //modify to use this with windows
	//debug_printf( "Successfully populated candidate stuff, time: %d ms\n", timer.GetTime() );
	//Timer timer2;
	//timer2.StartTimer(); 
        //The following section prints out the image stars with their remaining
        //catalog stars, including the votes for each.
#if 0
        for (int j = 0; j < ImageStars.size(); j++ ){
		printf("Image Star %d\n", j);
		candidate_map::iterator it;
		for ( it = StarCandidates[j].begin(); it != StarCandidates[j].end(); it++ ){
                    //if(it->second > 3){
                        
			printf("	1) CatalogStars ID: %d | Votes: %d\n", it->first, it->second);
                    //}
		}
	}        
#endif

        //The next section prints the votes obtained by each image star for each catalog star.
#if 0
	for (int gil = 0; gil < ImageStars.size(); gil++ ){
		//printf("Image Star %d\n", gil);
		candidate_map::iterator it;
		for ( it = StarCandidates[gil].begin(); it != StarCandidates[gil].end(); it++ )
		{
                    if(it->second > 3){
                        
			printf("	1) CatalogStars ID: %d | Votes: %d\n", it->first, it->second);
                    }
		}
		/*for ( it = StarCandidates2[i].begin(); it != StarCandidates2[i].end(); it++ )
		{
			printf("	2) CatalogStars ID: %d | Votes: %d\n", it->first, it->second);
		}*/
	}
#endif
        
        //For each image star, catalog stars with insufficient votes are purged
        //NOTE: I noticed that erasing in a vector requires shifting the elements
        //after the erased element. This results in a longer run-time.
        //I recommend using a different technique, such as creating a new map.
        //I have implemented this in the next section of code
#if 0
        for (int j = 0; j < ImageStars.size(); j++ ){
		//printf("Image Star %d\n", gil);
		candidate_map::iterator it;
                candidate_map::iterator it2;
                //unsigned int sz = StarCandidates[gil].size();
                
		for ( it = StarCandidates[j].begin(); it != StarCandidates[j].end(); it++){
                    // the below code involving it2 is just a hack to get around some limited functionality
                    // of the STD map
                    if(it->second < OK_VOTES){
                        it2 = it;
                        it++;
                        StarCandidates[j].erase(it2);
                    } else {
                        it++;
                    }
		}
	}
#endif
        
         //In the next section, I add the values of the catalog stars with
        //sufficiently high votes to candidates_vec

#if 0
        for (int j = 0; j < ImageStars.size(); j++ ){
		candidate_map::iterator it;
                
		for ( it = StarCandidates[j].begin(); it != StarCandidates[j].end(); it++){ 
                    if(it->second > OK_VOTES_1 - 1)
                        candidates_vec[j].push_back( candidate_vector::value_type (it->first, 0) );
		}
	}
        
#endif
        
        //Now, we repeat the algorithm, checking only catalog stars with 
        //sufficiently high votes
#if 0
        for (int j = 0; j < ImageStars.size()-1; j++ )
	{
		for (int k = j+1; k < ImageStars.size(); k++ )
		{
			candidate_vector::iterator it1, it2;
			for ( it1 = candidates_vec[j].begin(); it1 != candidates_vec[j].end(); it1++ )
			{
				for ( it2 = candidates_vec[k].begin(); it2 != candidates_vec[k].end(); it2++ )
				{
					index_t pair_index = j*ImageStars.size() + k - (j+1)*(j+2)/2;
					sfloat dot = dot_product(CatalogStars[it1->first].r, CatalogStars[it2->first].r);
					sfloat lower = ImagePairs[pair_index].lower;
					sfloat upper = ImagePairs[pair_index].upper;
					if ( dot >= lower && dot <= upper )
					{
						// both stars good, add vote to current map
						it1->second += 1;
						it2->second += 1;
					}
				}
			}
		}
	}
#endif
        //PrintCurrentVotes(ImageStars, candidates_vec);
        
#if 0
        //TEMPORARY
        //This will be replaced by a more robust algorithm. Now it just picks
        //the first element, for the purpose of demonstrating we can retrieve 
        //the solution.
        for (int j = 0; j < ImageStars.size(); j++ ){
                candidate_vector::iterator it;
                it = candidates_vec[j].begin();
                int curMax = 0;
                //cout << "------------------------" << endl;
                while(it != candidates_vec[j].end()){
                        if(it->second > curMax){
                            ImageStars[j].identity = it->first;
                            curMax = it->second;
                            //cout << it->first << "\t" << it->second << endl;
                        }
                        it++;
                }
                cout << "max picked\t" << curMax << endl;
                //cout << ImageStars[j].identity << "\t" << curMax << endl;
        }
        
#endif
        //repeat another iteration
        //implement later as separate method?
#if 0
        for (int j = 0; j < ImageStars.size(); j++ ){
		candidate_map::iterator it;
                
		for ( it = candidates_vec[j].begin(); it != candidates_vec[j].end(); it++){ 
                    if(it->second > OK_VOTES_2 - 1)
                        candidates_vec2[j].insert( candidate_map::value_type(it->first, 0) );
		}
	}
#endif  
        
#if 0
        for (int j = 0; j < ImageStars.size()-1; j++ )
	{
		for (int k = j+1; k < ImageStars.size(); k++ )
		{
			candidate_map::iterator it1, it2;
			for ( it1 = candidates_vec2[j].begin(); it1 != candidates_vec2[j].end(); it1++ )
			{
				for ( it2 = candidates_vec2[k].begin(); it2 != candidates_vec2[k].end(); it2++ )
				{
					index_t pair_index = j*ImageStars.size() + k - (j+1)*(j+2)/2;
					sfloat dot = dot_product(CatalogStars[it1->first].r, CatalogStars[it2->first].r);
					sfloat lower = ImagePairs[pair_index].lower;
					sfloat upper = ImagePairs[pair_index].upper;
					if ( dot >= lower && dot <= upper )
					{
						// both stars good, add vote to current map
						it1->second += 1;
						it2->second += 1;
					}
				}
			}
		}
	}
#endif
        
        //PrintCurrentVotes(ImageStars, candidates_vec2);

        
        /*for (int gil = 0; gil < ImageStars.size(); gil++ ){
		printf("Image Star %d\n", gil);
		candidate_map::iterator it;
		for ( it = StarCandidates[gil].begin(); it != StarCandidates[gil].end(); it++ ){
                    //if(it->second > 3){
                        
			printf("	1) CatalogStars ID: %d | Votes: %d\n", it->first, it->second);
                    //}
		}
		for ( it = StarCandidates2[i].begin(); it != StarCandidates2[i].end(); it++ )
		{
			printf("	2) CatalogStars ID: %d | Votes: %d\n", it->first, it->second);
		}
	}
        
        for (int gil = 0; gil < ImageStars.size(); gil++ ){
		candidate_map::iterator it;
		for ( it = StarCandidates[gil].begin(); it != StarCandidates[gil].end(); it++ ){
                    
                }
        } */
     
        /*
#ifdef ONLY_ONE_STAGE
	int i;
	for ( i = 0; i < ImageStars.size(); i++ )
	{
#ifdef PRINT_TOP_VOTES
		printf("%03d) (x,y) = (%f, %f) | Candidates: %d\n", i, ImageStars[i].centroid_x, ImageStars[i].centroid_y, StarCandidates[i].size() );

		candidate_vector tmp_vec(StarCandidates[i].begin(), StarCandidates[i].end());
		sort( tmp_vec.begin(), tmp_vec.end(), compare_vector );
		candidate_vector::iterator it;
		for ( it = tmp_vec.begin(); it != tmp_vec.end() && int(it - tmp_vec.begin()) < 10; it++ )
			printf("	%03d Votes | CtgStar: %04d | (RA,DEC) = (%.15f, %.15f)\n",
					it->second, it->first, CatalogStars[it->first].RA, CatalogStars[it->first].Dec );

		it = tmp_vec.begin();
		if ( it != tmp_vec.end() && it->second >= 0.75f*ImageStars.size() )
			ImageStars[i].identity = it->first;
#else
		candidate_map::iterator it = min_element(StarCandidates[i].begin(), StarCandidates[i].end(), compare_map );
		if ( it != StarCandidates[i].end() && it->second >= 0.75f*ImageStars.size() )
		{
			ImageStars[i].identity = it->first;
#if 0
			printf("	%03d Votes | CtgStar: %04d | (RA,DEC) = (%.15f, %.15f)\n",
					it->second, it->first, CatalogStars[it->first].RA, CatalogStars[it->first].Dec );
#endif
		}
#endif
	}
#else
	candidate_vector* candidates_vec = new candidate_vector[ImageStars.size()];

	//int low_votes = int( LOW_VOTES_COEFF*ImageStars.size() );
        
        

#define NUM_CANDIDATES 5
#define NUM_PARTIAL_SORT 20

	int i, j;
	for ( i = 0; i < ImageStars.size(); i++ )
	{
		candidate_vector tmp_vec( StarCandidates[i].begin(), StarCandidates[i].end() );
		candidate_vector::iterator it_mid = ( tmp_vec.size() > NUM_PARTIAL_SORT ? tmp_vec.begin()+NUM_PARTIAL_SORT : tmp_vec.end() );
		partial_sort( tmp_vec.begin(), it_mid, tmp_vec.end(), compare_vector );

		int min_votes = 1;
		candidate_vector::iterator it;
		for (	it = tmp_vec.begin();
				it != it_mid && ( int(it - tmp_vec.begin()) < NUM_CANDIDATES || it->second >= min_votes );
				it++ )
		{
			candidates_vec[i].push_back( candidate_pair(it->first, 0) );
			if ( int(it - tmp_vec.begin()) == NUM_CANDIDATES-1 )
				min_votes = it->second;
		}
	}

	for ( i = 0; i < ImageStars.size()-1; i++ )
	{
		for ( j = i+1; j < ImageStars.size(); j++ )
		{
			candidate_vector::iterator it1, it2;
			for ( it1 = candidates_vec[i].begin(); it1 != candidates_vec[i].end(); it1++ )
			{
				for ( it2 = candidates_vec[j].begin(); it2 != candidates_vec[j].end(); it2++ )
				{
					index_t pair_index = i*ImageStars.size() + j - (i+1)*(i+2)/2;
					sfloat dot = dot_product(CatalogStars[it1->first].r, CatalogStars[it2->first].r);
					sfloat lower = ImagePairs[pair_index].lower;
					sfloat upper = ImagePairs[pair_index].upper;
					if ( dot >= lower && dot <= upper )
					{
						// both stars good, add vote to current map
						it1->second += 1;
						it2->second += 1;
					}
				}
			}
		}
	}

	//low_votes = int(0.8f*low_votes);

	int max_votes = 0;

	for ( i = 0; i < ImageStars.size(); i++ )
	{
		candidate_vector::iterator it_mid = (	candidates_vec[i].size() > NUM_PARTIAL_SORT ?
												candidates_vec[i].begin()+NUM_PARTIAL_SORT : candidates_vec[i].end() );
#ifndef PRINT_TOP_VOTES
		// assign identities and exit
		candidate_vector::iterator it = min_element( candidates_vec[i].begin(), candidates_vec[i].end(), compare_vector );
#else
		partial_sort( candidates_vec[i].begin(), it_mid, candidates_vec[i].end(), compare_vector );
		candidate_vector::iterator it = candidates_vec[i].begin();
#endif

		if ( it != it_mid && it->second > max_votes )
			max_votes = it->second;
	}

	if ( true ) //max_votes <= 4 )
	{
		debug_printf("Low votes detected, doing another stage\n");
		for ( i = 0; i < ImageStars.size(); i++ )
		{
			candidate_vector::iterator it_mid = (	candidates_vec[i].size() > NUM_PARTIAL_SORT ?
				candidates_vec[i].begin()+NUM_PARTIAL_SORT : candidates_vec[i].end() );

			candidate_vector::iterator it;
			for (	it = candidates_vec[i].begin();
					it != it_mid && (	( ImageStars.size() >= IMAGE_STARS_USE_MAX_CUTOFF && it->second >= int( MAX_VOTES_COEFF * max_votes ) ) ||
										( ImageStars.size() < IMAGE_STARS_USE_MAX_CUTOFF && it->second > 0 ) );
					it++ );

			candidates_vec[i].erase(it, candidates_vec[i].end());

			for ( it = candidates_vec[i].begin(); it != candidates_vec[i].end(); it++ )
				it->second = 0;
		}

		for ( i = 0; i < ImageStars.size()-1; i++ )
		{
			for ( j = i+1; j < ImageStars.size(); j++ )
			{
				candidate_vector::iterator it1, it2;
				for ( it1 = candidates_vec[i].begin(); it1 != candidates_vec[i].end(); it1++ )
				{
					for ( it2 = candidates_vec[j].begin(); it2 != candidates_vec[j].end(); it2++ )
					{
						index_t pair_index = i*ImageStars.size() + j - (i+1)*(i+2)/2;
						sfloat dot = dot_product(CatalogStars[it1->first].r, CatalogStars[it2->first].r);
						sfloat lower = ImagePairs[pair_index].lower;
						sfloat upper = ImagePairs[pair_index].upper;
						if ( dot >= lower && dot <= upper )
						{
							// both stars good, add vote to current map
							it1->second += 1;
							it2->second += 1;
						}
					}
				}
			}
		}

		max_votes = 0;

		for ( i = 0; i < ImageStars.size(); i++ )
		{
			candidate_vector::iterator it_mid = (	candidates_vec[i].size() > NUM_PARTIAL_SORT ?
				candidates_vec[i].begin()+NUM_PARTIAL_SORT : candidates_vec[i].end() );

#ifndef PRINT_TOP_VOTES
			// assign identities and exit
			candidate_vector::iterator it = min_element( candidates_vec[i].begin(), candidates_vec[i].end(), compare_vector );
#else
			partial_sort( candidates_vec[i].begin(), it_mid, candidates_vec[i].end(), compare_vector );
			candidate_vector::iterator it = candidates_vec[i].begin();
#endif

			if ( it != it_mid && it->second >= max_votes )
				max_votes = it->second;
		}
	}

	int low_votes = int( MAX_VOTES_COEFF * max_votes );

	debug_printf("Max Votes: %d | Low Votes: %d\n", max_votes, low_votes );

	for ( i = 0; i < ImageStars.size(); i++ )
	{
#ifndef PRINT_TOP_VOTES
		// assign identities and exit
		candidate_vector::iterator it = min_element( candidates_vec[i].begin(), candidates_vec[i].end(), compare_vector );
#else
		candidate_vector::iterator it = candidates_vec[i].begin();
#endif

		if ( low_votes >= 3 && it != candidates_vec[i].end() && it->second >= low_votes )
			ImageStars[i].identity = it->first;
		else
			ImageStars[i].identity = INDEX_INVALID;

		sfloat RA = 0.0f, DEC = 0.0f;
		if (ImageStars[i].identity != INDEX_INVALID && ImageStars[i].identity != INDEX_FALSE_STAR)
		{
			RA = CatalogStars[ImageStars[i].identity].RA;
			DEC = CatalogStars[ImageStars[i].identity].Dec;
		}

//#ifdef DEBUG_TEXT
//#ifdef PRINT_TOP_VOTES
 		/*printf("(%02d) Candidates: %d | Identity: %d | Votes: %d | (X,Y) = (%.3f,%.3f) | (RA,DEC) = (%f,%f)\n",
			i+1, candidates_vec[i].size(), ImageStars[i].identity, it->second,
			ImageStars[i].centroid_x, ImageStars[i].centroid_y, RA, DEC );
                candidates_vec[i].size();
                ImageStars[i].identity = ImageStars[i].identity;
                it->second = it->second;
                ImageStars[i].centroid_x = ImageStars[i].centroid_x;
                ImageStars[i].centroid_y = ImageStars[i].centroid_y;
                     

		/*for ( j = 0; it != candidates_vec[i].end() && j < 20; it++, j++ )
		{
			printf("	%04d Votes | CtgStar: %04d | (RA,DEC) = (%f, %f)\n",
				it->second, it->first, CatalogStars[it->first].RA, CatalogStars[it->first].Dec );
		}*/
//#endif /* PRINT_TOP_VOTES */
//#endif /* DEBUG_TEXT */
/*
	delete[] candidates_vec;
#endif
	delete[] StarCandidates;

	//timer2.StopTimer();
	//timer.StopTimer();

	//debug_printf("Successfully identified stars. | Time elapsed: %d ms | Total Time: %d ms\n", timer2.GetTime(), timer.GetTime());
#ifdef PROMPT_USER
	printf("Press ENTER to continue.\n");
	getchar();
#endif
        */
        
}	