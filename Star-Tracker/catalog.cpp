//		//Generating a Catalog:
//
//		//To initialize the catlaog, create a Catalog:
//		Catalog glCatalog;
//		
//		//You will need the system time:
//		
//		SYSTEMTIME systime;
//		GetSystemTime( &systime );
//
//		//We will also need the focal length. For our camera/lens, we used 4293.0f
//		
//		//Finally, initialize the Catalog. 
//
//		glCatalog.Initialize( "catalog.txt", &systime, FocalLength );
//
//
//
//


#include "catalog.h"
#include "Timer.h"
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <time.h>
#include <iostream>

using namespace std;

#define FOCAL_LENGTH    4293.0

//put somehting better, make compatible with windows
#define UNIX_BASED      true

// arguments in RADIANS (or RADIANS / YEARS). Takes RA,DEC,proper motion, and uses time to find new RA and DEC. Puts that in tempRA and tempDEC
static void updateCoordinates(double* RA, double* Dec, double pmRA, double pmDEC, double t, double zeta, double z, double theta)
{
	//printf("RA = %f Dec = %f\n", RA,Dec);

	*RA += pmRA * t * 100; 															//update RA and DEC due to proper motion.
	*Dec += pmDEC * t * 100;

	double A = cos(*Dec) * sin(*RA + zeta);
	double B = cos(theta) * cos(*Dec) * cos(*RA + zeta) - sin(theta) * sin(*Dec);
	double C = sin(theta) * cos(*Dec) * cos(*RA + zeta) + cos(theta) * sin(*Dec);	//A, B, and C are used for precession.

	//printf("A = %f B = %f C = %f\n",A,B,C);

	*RA = z + atan2(A,B);															//update RA and DEC based on precession.
	if (*RA < 0)
		*RA += 2 * M_PI;
	*Dec = asin(C);
}

//gets Julian date
double getJulianDate() {
    time_t rawtime;
    struct tm * ptm;
    time(&rawtime);
    ptm = gmtime(&rawtime);
    
    //cout << "year " << ptm->tm_year << endl;

    int A, B, C, E, F; //used for computing the Julian Date
    double JDN;

    A = (int) ( (ptm->tm_year+1900) / 100);
    B = (int) (A / 4);
    C = 2 - A + B;
    E = (int) (365.25 * ( (ptm->tm_year+1900) + 4716));
    F = (int) (30.6001 * (ptm->tm_mon + 2));
    JDN = (double) (C + E + F + ptm->tm_mday ) - 1524.5;

    return JDN + (double) ptm->tm_hour / 24 + (double) ptm->tm_min / (24 * 60)
            + (double) ptm->tm_sec / (24 * 60 * 60);
}


void ReadCatalog( vector<catalog_star>& Catalog, const char* filename)
{
	

	//vector<catalog_star> Catalog;
	Catalog.clear();
	Catalog.reserve(10000);

	//the following variables are used to compute precession:
        
        // how to get time on a Unix-based system
        //time_t unixSeconds;
        //time(&unixSeconds);
        
        
        //timer used by windows:
        //Timer timer;
	//timer.StartTimer();
        
        
        //universal timer (uses Timer.h and Timer.cpp files)
        Timer timer;
        timer.start();
        
        double julianDate = getJulianDate();
	//printf("julian_day: %8f\n", julianDate );
	double t = (julianDate - 2451545.0) / 36525.0; //time since J2000
	double zeta = ( 2306.2181*t + 0.30188*t*t + 0.017998*t*t*t ) / (60*60) * (M_PI/180);
	double z = ( 2306.2181*t + 1.09468*t*t + 0.018203*t*t*t ) / (60*60) * (M_PI/180);
	double theta = ( 2004.3109*t - 0.42665*t*t - 0.041833*t*t*t ) / (60*60) * (M_PI/180);

	//printf("t = %10f zeta = %f z = %f theta = %f\n",t,zeta,z,theta);

	FILE* file = fopen(filename, "r");
	if (file == NULL)
	{
		printf("Error: Could not open catalog.\n");
		//return Catalog;
		return;
	}
	
	char buffer[256];
        
        long count = 0;                                                     //used for test        

        
        while ( fgets(buffer, 256, file) != NULL )
	{
            /*              1666 Cursa
                            1713 Rigel
                            1899 Na'ir Al Saif
                            1788 Eta Ori
                            1852 Mintaka
                            1903 Alnilam
                            1948 Alnitak*/
            count++;
            
            //used to pick every second star, if we wish to restrict
            // TODO, right now it restricts the catalog in half arbitrarily,
            // we should restrict them by brightness, if at all
            //if (count % 2 != 0)
            //    continue;
            //used to pick only the 7 stars listed
           // if (count != 1666 && count != 1713 && count != 1899 && count !=1788      
             //       && count != 1852 && count != 1903 && count != 1948) //used for test
               // continue;
           //cout << count << endl;
		if ( strlen(buffer) >= 107 )
		{
			// read RA, Dec, apparent magnitude, proper motion
			int RA_hour, RA_min, DEC_deg, DEC_min, DEC_sec;
			float RA_sec, app_mag, pmRA, pmDec;
			char DEC_sign;
			char name[11];
			int ret = 0;
			ret += sscanf(	buffer+4,	"%10c", name );
			name[10] = '\0';
			ret += sscanf(	buffer+75,	"%2d%2d%4f%c%2d%2d%2d", &RA_hour, &RA_min, &RA_sec, &DEC_sign, &DEC_deg, &DEC_min, &DEC_sec );
			ret += sscanf(	buffer+102,	"%4f", &app_mag );
			ret += sscanf(	buffer+148,	"%6f%6f", &pmRA, &pmDec );
/*
			int ret = sscanf(	buffer, "%*75c%2d%2d%4f%c%2d%2d%2d%*13c%4f%*41c%6f%6f",
								&RA_hour, &RA_min, &RA_sec, &DEC_sign, &DEC_deg, &DEC_min, &DEC_sec, &app_mag, &pmRA, &pmDec );
*/

			//printf("(Original) %02d:%02d:%02.1f, %c%02d:%02d:%02d, %.2f, %+.3f, %+.3f\n", RA_hour, RA_min, RA_sec, DEC_sign, DEC_deg, DEC_min, DEC_sec, app_mag, pmRA, pmDec );

			// checks for incomplete data and ignores magnitudes higher than max
			if (ret == 11 && app_mag <= MAX_MAGNITUDE )
			{
				double RA = 2 * M_PI * ( (sfloat)RA_hour/24 + (sfloat)RA_min/(24*60) + (sfloat)RA_sec/(24*60*60) );
				double DEC = M_PI * ( (sfloat)DEC_deg/180 + (sfloat)DEC_min/(180*60) + (sfloat)DEC_sec/(180*60*60) );
				if (DEC_sign == '-')
					DEC = -DEC;

				// units are arcsec/yr, convert arcsec -> deg -> rad
				double pmRA_d = pmRA / (60*60) * (M_PI/180);
				double pmDec_d = pmDec / (60*60) * (M_PI/180);

				//printf( "(Converted) RA: %f, Dec: %f, AppMag: %.2f, pmRA: %.15f, pmDec: %.15f\n", star.RA, star.Dec, star.app_mag, pmRA, pmDec );
				updateCoordinates( &RA, &DEC, pmRA_d, pmDec_d, t, zeta, z, theta );

				//give each star its properties
				catalog_star star;
				star.RA = (sfloat)RA;
				star.Dec = (sfloat)DEC;
				star.app_mag = app_mag;
				star.r[0] = cos(star.Dec)*cos(star.RA);
				star.r[1] = cos(star.Dec)*sin(star.RA);
				star.r[2] = sin(star.Dec);
				strcpy(star.name, name);
                                
                                //printf( "(Updated) RA: %f, Dec: %f, AppMag: %.2f\n", star.RA, star.Dec, star.app_mag );


				//put the star in the catalog.
				Catalog.push_back(star);

			}
		}
	}

	fclose(file);

        //timer when using windows
	//timer.StopTimer();
        
        timer.stop();

#if 0
	vector<catalog_star>::iterator it;
	for ( it = Catalog.begin(); it != Catalog.end(); it++ )
		printf(	"%04d | (RA, Dec) = (%f, %+f) | AppMag: %.2f | r = (%+f,%+f,%+f)\n",
				int(it - Catalog.begin()), it->RA, it->Dec, it->app_mag, it->r[0], it->r[1], it->r[2] );
#endif

	//printf("Catalog successfully read, size: %d | Time elapsed: %d ms\n", Catalog.size(), timer.getElapsedTimeInMicroSec(), NULL);
#ifdef PROMPT_USER
	printf("Press ENTER to continue.\n");
	getchar();
#endif

	//return Catalog;
}

bool compare_catalog(catalog_pair first, catalog_pair second)
{
	return (first.distance < second.distance);
}

// Puts the sorted catalog star pairs into CatalogPairs, excluding double stars and pairs farther than a certain distance away
void GetSortedCatalogPairs( vector<catalog_pair>& CatalogPairs, vector<catalog_star>& Catalog, float FocalLength )
{

	const sfloat dot_lower = cos( MAX_ANG_DIST );											//bound on large angles can't see both stars in the same image.
	const sfloat dot_upper = cos( atan((sfloat)DOUBLE_STAR_PIXEL_RADIUS/FocalLength) );		//bound on small angles to avoid double stars

	debug_printf("Creating catalog pairs in Range = [%f, %f]\n", dot_lower, dot_upper );

	//vector<catalog_pair> CatalogPairs;
	CatalogPairs.clear();
	CatalogPairs.reserve(1000000);

	index_t i, j;
	for ( i = 0; i < Catalog.size() - 1; i++ )												//first star
	{
		for ( j = i + 1; j < Catalog.size(); j++ )											//second star
		{
			sfloat dot = dot_product(Catalog[i].r, Catalog[j].r);
			if ( dot_lower <= dot && dot <= dot_upper)										//the conditions are met
			{
				catalog_pair pair = { i, j, dot };											//make a pair
				CatalogPairs.push_back(pair);												//add the pair to the vector
			}

			//printf("(%04d,%04d) Dot: %+.17f | AngDist: %+.17f\n", i, j, dot, acos(dot));
		}
	}

	sort( CatalogPairs.begin(), CatalogPairs.end(), compare_catalog );						//sort the vector by distances.


#if 0
	vector<catalog_pair>::iterator it;
	for ( it = CatalogPairs.begin(); it != CatalogPairs.end(); it++ )
		printf("%05d (%04d, %04d) AngDist: %.17f\n", int(it - CatalogPairs.begin()), it->star1, it->star2, it->ang_dist);
#endif

	debug_printf("Successfully created and sorted CatalogPairs, size: %d | Time elapsed: %d ms\n", CatalogPairs.size(), timer.GetTime());
#ifdef PROMPT_USER
	printf("Press ENTER to continue.\n");
	getchar();
#endif

	//return CatalogPairs;
}

//put MN --> N, for 3x3 matrices M,N
void Apply_Matrix(double M[], double N[])
{	
	//store values of N in the matrix L
	double L[9];
	for(int i = 0; i < 9; i++)
	{
		L[i] = N[i];
		N[i] = 0;
	}

	//perform the operation N_{ij} = sum_k M_{ik} L{kj}
	for(int i = 0; i < 3; i++)
	for(int j = 0; j < 3; j++)
	for(int k = 0; k < 3; k++)
		N[3*i + j] += M[3*i + k] * L[3*k + j];

	return;
}

//put Mx --> y for 3x3 matrix M and vector x.
void Apply_Vector(double M[], double x[], double y[])
{	
	//perform the operation x_{i} = sum_k M_{ik} y{k}
	for(int i = 0; i < 3; i++)
	for(int k = 0; k < 3; k++)
		y[i] += M[3*i + k] * x[k];

	return;
}

//put the transpose of R in RT
void Transpose(double R[], double RT[])
{
	for(int i = 0; i < 3; i++)
	for(int j = 0; j < 3; j++)
		RT[3*i + j] = R[3*j + i];

	return;
}

//uses RA,DEC,T to find the rotation matrix that describes the coordinates system of the spacecraft
//Apply R^T to a star's (x,y,z) coordinates in the sky to find its coordinates with respect to the spacecraft.

void Standard_Rotation(double RA, double DEC, double T, double R[])
{
	double R_x [] = {1,		0,	0, 
			0,		cos(T),	sin(T), 
			0,		-sin(T),cos(T)};

	double R_y [] = {cos(DEC),	0,	-sin(DEC),
			0,		1,	0,
			sin(DEC),	0,	cos(DEC)};

	double R_z [] = {cos(RA),	-sin(RA),	0, 
			sin(RA),	cos(RA),	0, 
			0,		0,		1};

	//compute the rotation matrix that describes the attitude of the spacecraft
	Apply_Matrix(R_x, R);
	Apply_Matrix(R_y, R);
	Apply_Matrix(R_z, R);

	return;
}

//TEST ME
//This method takes r[], alpha, and gamma, and finds the plate values X and Y
//Note we will use this method with r_craft[], the coordinates of r in the new frame

void To_Plate(double r[], double &X, double &Y, double alpha, double gamma)
{
	//Compute the values of X and Y without the twist angle gamma
	double Q = r[0]*sin(alpha) + r[2]*cos(alpha);
        
        //Q is the dot-product of the vector describing the image center and 
        //each star. If Q is negative, the stars are on the opposite side,
        //and we take them out of the image.
        if (Q < 0)
        {
            X = 9999;
            Y = 9999;
            return;
        }
        //cout << "r:" << r[0] << " " << r[1] << " " << r[2] << endl;
	X = -r[1]*FOCAL_LENGTH / Q;
        
	Y = FOCAL_LENGTH * sqrt( pow(r[0]/Q - sin(alpha), 2) + pow(r[2]/Q - cos(alpha), 2) );
	//Flip the sign of Y if necessary. The first condition checks which of the star and origin
	//is higher in the plane (along the z axis). The other two conditions are in place in case 
	//the plane is flat. They use the positions along the x-axis instead.
	if (r[2]/Q - cos(alpha) < 0 || (abs(fmod(alpha,2*PI)) < 0.0001 && r[0]/Q - sin(alpha) < 0) ||  (abs(fmod(alpha - PI,2*PI)) < 0.0001 && r[0]/Q - sin(alpha) > 0) )
		Y = -Y;

	
	//twist X and Y by the angle gamma
	double X_temp = X, Y_temp = Y;
	X = cos(gamma)*X_temp + sin(gamma)*Y_temp;
	Y = -sin(gamma)*X_temp + cos(gamma)*Y_temp;
        	
	return;
}

//TEST ME
// This method calls the various methods needed to take RA,DEC,T, create the matrix R, 
// and use alpha and gamma to find r_craft for each star using r.
// It them updates the plate coordinates X,Y for each star.
void Catalog::update_all(double alpha, double gamma, double RA, double DEC, double T)
{

	double R[]   = {1,		0,		0,
			0,		1,		0,
			0,		0,		1};
	double RT[9];

	// We find the rotation telling us where the starcraft is pointing
	Standard_Rotation(RA,DEC,T,R);
	// transpose for matrix bringing the stars to out new frame
	Transpose(R,RT);

    vector<catalog_star>::size_type sz = Stars.size();         // this loop and print was for debugging purposes
    unsigned int i;

	for (i=0; i<sz; i++)
	{
            Stars[i].r_craft[0] = 0;
            Stars[i].r_craft[1] = 0;
            Stars[i].r_craft[2] = 0;
            
                //cout << "r " << Stars[i].r[0] << " " << Stars[i].r[1] << " " << Stars[i].r[2] << " " << endl;

		// rotate, updates r_craft for each star
		Apply_Vector(RT,Stars[i].r, Stars[i].r_craft);
                
		//cout << "intended r_craft: " << Stars[i].r_craft[0] << " " << Stars[i].r_craft[1] << " " << Stars[i].r_craft[2] << " " << endl;

		// find X,Y and update them for each star
		To_Plate(Stars[i].r_craft, Stars[i].X,Stars[i].Y, alpha, gamma);
		//To_Plate(Stars[i].r, Stars[i].X,Stars[i].Y, alpha, gamma);
	}

	return;

}

Catalog::Catalog()
{
	initialized = false;
}

// extending the class to be cross platform
void Catalog::Initialize( const char* filename, float FocalLength )
{
	ReadCatalog( Stars, filename );
	GetSortedCatalogPairs( SortedPairs, Stars, FocalLength );
	initialized = true;
}
