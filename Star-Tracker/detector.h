#include "star.h"
#include <vector>
#include "star_map.h"

#pragma once
//This class is able to detect stars using the data from the star_map class.

class detector {
    
public:
    detector(int gl_sample_skip, int local_width, int local_height,
            int local_sample_skip, int star_min_outstnd, sfloat mean_sky);
    size_t DetectStars(vector<image_star>& ImageStars, star_map* Image);
    
    sfloat mean_sky;
private:
    //coordinates of pixels used when finding stars.
    //the third component represents the (l2) distance of said pixel and closest marked pixel
    // (0 if the pixel is itself marked).
//    typedef tuple<short,short,sfloat> cart_coords;
    struct cart_coords {
        short x, y; sfloat D, value;
        bool operator<(const cart_coords& a) const{
                return value < a.value;}
    };
    //determines if a pixel is marked
    static inline bool is_marked(star_map* Image, int x, int y);
    //mark a pixel
    static inline void mark(star_map* Image, int x, int y);
    inline sfloat error_function(sfloat phi);
    //find the mean over the area of radius r
    inline sfloat mean_middle_half( vector<cart_coords>& ind);
    static inline sfloat mean_over_area(star_map* Image, int x, int y, int radius);
    //find the median value in the annulus specified by the two radii
    static inline sfloat median_over_annulus(star_map * Image, int x, int y, int outter_radius, int inner_radius);
    //detect if the pixel (x,y) in the image is an outstanding pixel
    static inline bool isOutstanding( star_map* Image, int x, int y, double outst_val, double low_val );
    //the recursive algorithm used to find stars. Basically, it detects
    //outstanding pixels that are grouped together.
    static void detector_recurse(star_map* Image, double low_val, double outst_val, double mean,
                                int x, int y, int* total_count, int* outstnd_count,
                                long long* x_numer, long long* y_numer, long long* sum );
    static void detector_recurse(star_map * Image, double low_val, double outst_val, double mean,
                                int x, int y, vector<cart_coords>& ind, bool diag, sfloat D );
    //finds the centroid of a group of outstanding pixels, based at x,y.
    inline void CentroidAt(star_map* Image, vector<image_star>& ImageStars,
                                double low_val, double outst_val, double mean, int x, int y );
    //finds the centroid of a group of outstanding pixels, based at x,y. Uses apeture photometry
    inline void CentroidAtAperture(star_map* Image, vector<image_star>& ImageStars,
                                double low_val, double outst_val, double mean, int x, int y );
    bool compare_radius(image_star& left, image_star& right);
    	
    int gl_sample_skip;		// distance to next pixel in global sample
    int local_width;		// box width for local mean and standard deviation
    int local_height;		// box height for above
    int local_sample_skip;	// distance to next pixel in local sample
    int star_min_outstnd;
    static vector<bool> marked;
};
/*
struct image_s {
	int width;
	int height;
	int bitmode;
	float FocalLength;
	void* data;
	void* out;
};

struct detector_s {
	int gl_sample_skip;		// distance to next pixel in global sample
	int local_width;		// box width for local mean and standard deviation
	int local_height;		// box height for above
	int local_sample_skip;	// distance to next pixel in local sample
	int star_min_outstnd;
	sfloat mean_sky;
};*/

// ImageStars -- vector to be cleared and filled with image stars
// Image -- pointer to star_map containing image data & attributes
// returns number of stars detected in image, not number of stars in data structure (usually gets truncated)