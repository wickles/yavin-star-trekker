/*
 *
 *  This class provides a clean interface for dealing with images of the sky. 
 *  Designed for Star Tracker Project.
 *  Karanbir Toor & Gil Tabak
 *  October 20, 2012
 *
 */

// The purpose of the following classes/methods is to store the pixel values.
// The data array is used to store pixel values. it keeps an array with 2 * number of pixels, two entries for each pixel.
// Each value in the array is a short unsigned char, so that each pixel has 16 bits max.
// We do this to easily write into FITS format.
// We are using big-endian notation, so the first unsigned char of a pixel contains the bigger values.
// in addition to the standard get/set methods, we also have add which adds new values to existing values.
// Note the set method will not allow pixel values of less than 0 or bigger than 2^16 - 1.
// There are also two print functions for de-bugging. One is for all characters and the other for pixel values

// The center of the image is at (-0.5, -0.5) (assuming width and height are even)
// example: width = 1280, height = 960. Then the coordinates span [-640,639],[-480,479].

#ifndef star_map_h
#define star_map_h

#include <stdio.h>
#include <iostream>
#include <stdint.h>
#include "catalog.h"
#include <cmath>
#include <assert.h>
#include <fstream>
#include <cstdlib>


#define PI              3.14159265358979323846264338327950288

using namespace std;

#define CAMERA_WIDTH	1280
#define CAMERA_HEIGHT	960
#define FOCAL_LENGTH    4293.0
#define BRIGHTNESS      1000000.0 //scale for signal of each star in the image.
#define SD_DEV          1.5 //the standard deviation used in generating stars
#define HEADER_SIZE     2880
#define MIN_VAL         0
#define MAX_VAL         65535

class catalog;

class star_map {
    
public:
    star_map(double ra, double dec, double tw, double alpha, double gamma, float garbage,
                int lambda,const char* f);
                                                                    // Constructor. same 6 arguments same as below.
                                                                    // also adds noise lambda and prints to file f
    star_map(double ra, double dec, double tw, double alpha, double gamma, float garbage);          
                                                                    // constructor, the first three are coordinates of spacecraft, 
                                                                    // alpha and gamma are coordinates of the cammera relative to 
                                                                    // the spacecraft, lastly a pointer to the catalogue for stars
    
    star_map(const char* f);                                        // This constructor reads in a fits file. VERY IMPORTANT, we 
                                                                    // assume the header is 2880 bytes long, this can vary in general
                                                                    // and this code has NOT been made to handle that.
    ~star_map();                                                       
    void printTo_FIT_file(const char* f);                                          // Prints a FIT file equivalent. Under construction.
    void Draw_Star(double X, double Y, double stDev, double mag);
    double abs_mag(double rel_mag);
    void print_HISTOGRAM();
    int get(int i, int j);
    void set(int i, int j, int value);
    
    // Used to add noise to the entire image. The values of the noise are assumed to take a Poisson distribution.
    void Add_Poisson_Noise(int lambda);
    void printImageStars();
    void printGeneratedImageStars();
    void printAlgorithmData();
    int height;
    int width;
    double FocalLength;
    Catalog* getRefToCatalog();
    vector<image_star> ImageStars;
    vector<image_star> GeneratedImageStars;
    coordinates BoresightOut; //used later to find the boresight angle
    coordinates CoordOut; //the RA and DEC of the image center determined
    sfloat RMat[3][3]; //matrix representing rotation determined
    double RQuat[4]; //quaternions representing rotation determined
    //TODO temporarily moved the data array to here to test if read/write is not corrupting the fit file.
    //should move back to private
    unsigned char* data;
    void populateImageStars(prior_s Prior);
private:
    inline double wrapAngle( double angle );
    Catalog glCatalog;
    int index(int i, int j);
    void copyToDataWithoutHeader(char* ary);
    void add(int i, int j, int value);
    double brightness;
    
    int DrawCounter;
    
};

//returns a number from  poisson distribution with mean lambda
double psn(int lambda);

#endif
