/* 
 * File:   static_algorithm.cpp
 * Author: Karan
 * 
 * Created on January 13, 2013, 6:37 PM
 */

#include "static_algorithm.h"
//#include "atmosphere.h"

using namespace std;

void static_algorithm::runAlgorithm(star_map* s){     
        if ( !s->getRefToCatalog()->initialized )
                return;
     
        // instantiate classes used for algorithm
        detector Detector(GL_SAMPLE_SKIP,LOCAL_WIDTH,LOCAL_HEIGHT,LOCAL_SAMPLE_SKIP,STAR_MIN_OUTSTND,0.0);
        identifier Identifier;
        
        coordinates Image_Coords = {0, 0};
        
        bool Prior_Valid = false;
        prior_s Prior;
        
        bool Image_Correct_ID = false;
   
        // FOR TESTING; BYPASS DETECTOR PHASE
        int num_detected = Detector.DetectStars(s->ImageStars, s);
        //int num_detected = s->ImageStars.size();
        float Mean_Sky_Level = (float)Detector.mean_sky;
   
        // FOR TESTING: use GeneratedImageStars instead.
        if(s->ImageStars.size() < 3){
                cout << "Not enough stars to continue.\n";
                return;
        }
        
        //----- debugging shtuff -------
        sfloat centroid_x, centroid_y;
	sfloat r_prime[3];
	sfloat r[3];
	sfloat error;
	sfloat radius;
	index_t identity;
        
        /*std::vector<image_star>::size_type sz = s->ImageStars.size();
        for(unsigned int i=0;i<sz;i++){
             cout << "-----------------------------------------------\n";
             
             cout << "(centroid_x, centroid_y)" << endl;
             cout << "\t" << "(" << s->ImageStars[i].centroid_x << "," << s->ImageStars[i].centroid_y << ")\n";
             
             cout << endl << "r_prime\n";
             cout << "\t" << s->ImageStars[i].r_prime[0] << "," << s->ImageStars[i].r_prime[1] << "," << s->ImageStars[i].r_prime[2] << endl;
             
             cout << endl << "r\n";
             cout << "\t" << s->ImageStars[i].r[0] << s->ImageStars[i].r[1] << s->ImageStars[i].r[2] << endl;
             
             cout << "error" << endl;
             cout << "\t" << s->ImageStars[i].error << endl;
             
             cout << "radius" << endl;
             cout << "\t" << s->ImageStars[i].radius << endl;
             
             cout << "identity" << endl;
             cout << "\t" << s->ImageStars[i].identity << endl;
             
             cout << "-----------------------------------------------\n";
        }   */    
        //------------------------------
        
        // enough stars, continue with identification
        //FOR TESTING ID: Use GeneratedImageStars
        Catalog* t = s->getRefToCatalog();
        Identifier.IdentifyImageStars(s->ImageStars, t, ( Prior_Valid ? &Prior : NULL ) );
        
	double rms_error = GetAttitude(s->ImageStars, s->getRefToCatalog()->Stars, &s->BoresightOut, &s->CoordOut, s->RMat, s->RQuat);
	
        Image_Correct_ID = ( rms_error <= 0.01 );
        
        cout << "rms_error\n" << "\t" << rms_error << endl;

        //stuff to do when we get a correct image.
        //update the prior coordinates and value,
        //get local coordinates from boresight angle,
        //get transpose of R for later
	/*if ( Image_Correct_ID )
	{
                Prior.coords = s->CoordOut;
		Prior_Valid = true;

                //TODO
		// Get Az, El. This part uses methods from the atmosphere file
		/*SYSTEMTIME systime;
		GetSystemTime( &systime );
		double JDN = getJulianDate( &systime );
		double LAT = Settings.Latitude * M_PI / 180;
		double LONG = Settings.Longitude * M_PI / 180;
		//printf("Julian: %f\n", JDN);
                Image_LST = getLST( LONG, JDN );
		Image_HA = getHA1( Image_LST, Coords.RA );
		Image_Azi = getAzi( Image_HA, LAT, Coords.DEC );
		Image_Ele = getEle( Image_HA, LAT, Coords.DEC );
		//Ele_Corr = correctEle( );
                 

                //find the Boresight Angle from BoresightCoords
                double HA, AZI;
		HA = getHA1( Image_LST, s->BoresightOut.RA );
                AZI = getAzi( HA, Settings.Latitude, s->BoresightOut.DEC );
		Image_Bore = acos ( cos(Image_Azi) * sin(AZI) - sin(Image_Azi) * cos(AZI) );

                //the transpose of R is used later to get the coordinates
                //from a specific pixel on the screen.
        }*/
}

/*if ( glCatalog.initialized ){
				detector_s Detector = {
					GL_SAMPLE_SKIP,
					LOCAL_WIDTH,
					LOCAL_HEIGHT,
					LOCAL_SAMPLE_SKIP,
					STAR_MIN_OUTSTND,
					0.0
				};

				num_detected = DetectStars(ImageStars, &Detector, &Image);
				//printf( "	Number of Stars Detected: %d\n", num_detected );
				Mean_Sky_Level = (float)Detector.mean_sky;

				if (ImageStars.size() >= 3)
				{
					// enough stars, continue with identification
					IdentifyImageStars(ImageStars, glCatalog, ( Prior_Valid ? &Prior : NULL ));

					coordinates Coords, BoresightCoords;
					sfloat R[3][3];
					sfloat RQuat[4];
					double rms_error = GetAttitude(ImageStars, glCatalog.Stars, &BoresightCoords, &Coords, R, RQuat);
					Image_Correct_ID = ( rms_error <= 0.01 );

					if ( Image_Correct_ID )
					{
						Prior.coords = Coords;
						Prior.tickCount = GetTickCount();
						Prior_Valid = true;

						Image_Coords = Coords;

						// Get Az, El
						SYSTEMTIME systime;
						GetSystemTime( &systime );
						double JDN = getJulianDate( &systime );
						double LAT = Settings.Latitude * M_PI / 180;
						double LONG = Settings.Longitude * M_PI / 180;
						//printf("Julian: %f\n", JDN);
						Image_LST = getLST( LONG, JDN );
						Image_HA = getHA1( Image_LST, Coords.RA );
						Image_Azi = getAzi( Image_HA, LAT, Coords.DEC );
						Image_Ele = getEle( Image_HA, LAT, Coords.DEC );
						//Ele_Corr = correctEle( );

						//find the Boresight Angle from BoresightCoords
						double HA, AZI;
						HA = getHA1( Image_LST, BoresightCoords.RA );
						AZI = getAzi( HA, Settings.Latitude, BoresightCoords.DEC );
						Image_Bore = acos ( cos(Image_Azi) * sin(AZI) - sin(Image_Azi) * cos(AZI) );

						int i, j;
						for ( i = 0; i < 3; i++ )
							for ( j = 0; j < 3; j++ )
								Image_Rt[i][j] = R[j][i];
					}
					else if ( Prior_Valid && GetTickCount() - Prior.tickCount >= PRIOR_TIMEOUT*1000 )
					{
						Prior_Valid = false;
					}

					if ( Settings.DumpData )
					{
						// Write data to output.txt
						FILE* file = fopen(output_name, "a");
						if ( file != NULL )
						{
							// change to CSV output
							if ( Image_Correct_ID )
							{
								coords_discrete coords;
								GetDiscreteCoords(&Coords, &coords);
								fprintf(file, "%02d/%02d/%04d "
											  "%02d:%02d:%02d.%03d,"
											  "%.4f, %.4f, %.1f,"
											  "%.6f, %.6f,"
											  "%02d:%02d:%f,"
											  "%02d:%02d:%f,"
											  "%.4f,%.4f,%d,"
											  "%.1f,%d,"
											  "%f,%f,%f,%f\n",
										localtime.wMonth, localtime.wDay, localtime.wYear,
										localtime.wHour, localtime.wMinute, localtime.wSecond, localtime.wMilliseconds,
										Settings.Latitude, Settings.Longitude, Settings.Altitude,
										Coords.RA*180/M_PI, Coords.DEC*180/M_PI,
										coords.RA_hr, coords.RA_min, coords.RA_sec,
										coords.DEC_deg, coords.DEC_min, coords.DEC_sec,
										Image_Azi*180/M_PI, Image_Ele*180/M_PI, Ele_Corr,
										Detector.mean_sky, num_detected,
										RQuat[0], RQuat[1], RQuat[2], RQuat[3] );

								
		//fprintf(file, "MM/DD/YYYY Time,LAT,LONG,ALT,RA,DEC,RA_hr,DEC_deg,AZI,ELE,AtmCorr,MeanSky,NumDetected,Rt00,Rt01,Rt02,Rt10,Rt11,Rt12,Rt20,Rt21,Rt22\n");
							}
							else
							{
								fprintf(file, "%02d/%02d/%04d "
											  "%02d:%02d:%02d.%03d,"
											  "%.4f, %.4f, %.1f,"
											  "ERR,ERR,ERR,ERR,"
											  "ERR,ERR,ERR,"
											  "%.1f,%d,"
											  "ERR,ERR,ERR,ERR\n",
										localtime.wMonth, localtime.wDay, localtime.wYear,
										localtime.wHour, localtime.wMinute, localtime.wSecond, localtime.wMilliseconds,
										Settings.Latitude, Settings.Longitude, Settings.Altitude,
										Detector.mean_sky, num_detected );
							}
							fclose(file);
						}
					}
				}
				else
				{
					printf("	Not enough stars to continue.\n");
				}
			}
		} // Settings.ProcessImages
#ifndef SDL_ENABLED
		
		// RA = hour.6, DEC = deg.4, AZI,ELE = deg.4
		if ( Image_Correct_ID )
			printf( "(RA, DEC) = (%.6f, %.4f) | (AZI, ELE) = (%.4f, %.4f) | Boresight: %.4f | Stars: %d | Mean: %.1f\n"
					"EleCorr: %d | LST: %f | HA: %f | (LAT, LONG) = (%.4f,%.4f) | ALT = %.1f\n",
					Image_Coords.RA*12/M_PI, Image_Coords.DEC*180/M_PI,
					Image_Azi*180/M_PI, Image_Ele*180/M_PI, Image_Bore*180/M_PI,
					num_detected, Mean_Sky_Level,
					Ele_Corr, Image_LST, Image_HA,
					Settings.Latitude, Settings.Longitude, Settings.Altitude );
		else
			printf( "(RA, DEC) = ERR | (AZI, ELE) = ERR | Boresight: ERR | Stars: %d | Mean: %.1f\n"
					"EleCorr: ERR | LST: ERR | HA: ERR | (LAT, LONG) = (%.4f,%.4f) | ALT = %.1f\n",
					num_detected, Mean_Sky_Level,
					Settings.Latitude, Settings.Longitude, Settings.Altitude );*/
