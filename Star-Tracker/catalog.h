/* catalog.cpp
Alexander Wickes and Gil Tabak
September 20, 2010
*/

#include "star.h"
#include <vector>
#include <time.h>
#include <math.h>

#pragma once

#define PI              3.14159265358979323846264338327950288

/*
This structure is meant to contain a star catalog and an associated set of star pairs,
which are loaded when Initialize is called. The catalog is loaded from the entries in
catalog.txt, excluding stars with magnitude higher than the value specified in star.h.
Pairs are created for all unique pair of catalog stars 
*/
struct Catalog {
	bool initialized;
	std::vector<catalog_star> Stars;
	std::vector<catalog_pair> SortedPairs;

	Catalog();
	void Initialize( const char* filename, float FocalLength );
        void update_all(double alpha, double gamma, double RA, double DEC, double T);
//        void GetSortedCatalogPairs( vector<catalog_pair>& CatalogPairs, vector<catalog_star>& Catalog, float FocalLength );
//        bool compare_catalog(catalog_pair first, catalog_pair second);
};
