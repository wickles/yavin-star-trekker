/* detector.cpp
Alexander Wickes and Gil Tabak
September 20, 2010
 */

#include "detector.h"
#include <cstdio>
#include <cmath>
#include <algorithm>
//modify timer for windows
//#include "time.h"
#include "star_map.h"

//put somehting better, make compatible with windows
//#define UNIX_BASED true

using namespace std;

vector<bool> detector::marked;

// This global was made for testing purposes and should 
// be commented out upon release along with areas flagged
// as /* TEST */
const char* f = NULL;
star_map* filteredMap = new star_map(NULL);
//--------------------------------------------------------

const int MEAN_VALUE_RADIUS = 2;
const double OUTST_MULT = 5.0;
const double LOW_VAL_MULT = 2.0;
const double ERROR_MULTIPLIER = 3.0; //constant to multiply the possible error of each star

detector::detector(int gl_spl_skp, int lcl_wdt, int lcl_hgt, int lcl_spl_skp, int str_min_out, sfloat mn_sky) {
    gl_sample_skip = gl_spl_skp;
    local_width = lcl_wdt;
    local_height = lcl_hgt;
    local_sample_skip = lcl_spl_skp;
    star_min_outstnd = str_min_out;
    mean_sky = mn_sky;
};


/*
static inline short get_pixel(star_map * Image, int x, int y)
{
        if (Image->bitmode == 8)
                return (short)((unsigned char*)Image->data)[y*Image->width+x];
        return ((short*)Image->data)[y*Image->width+x];
}

static inline void set_pixel(star_map * Image, int x, int y, short val)
{
        if ( Image->out == NULL )
                return;
        if (Image->bitmode == 8)
                ((unsigned char*)Image->out)[y*Image->width+x] = (unsigned char)val;
        ((short*)Image->out)[y*Image->width+x] = val;
}*/

// index function is used to map coordinates to our marked vector. 

int index(int i, int j) {
    return ( CAMERA_WIDTH * (j + CAMERA_HEIGHT / 2) + (i - CAMERA_WIDTH / 2));
}

inline bool detector::is_marked(star_map* Image, int x, int y) {
    if (x < -Image->width / 2 || x > Image->width / 2 || y < -Image->height / 2 + 1 || y > Image->height / 2 + 1) {
        cout << "fail\n";
        return true;
    }
    return marked[index(x, y)];
}

inline void detector::mark(star_map* Image, int x, int y) {
    if (x < -Image->width / 2 || x > Image->width / 2 || y < -Image->height / 2 + 1 || y > Image->height / 2 + 1) {
        cout << "fail\n";
        return;
    }
    marked[index(x, y)] = true;
}

inline sfloat detector::error_function(sfloat phi) {
    return ERROR_FUNCTION(phi);
}

inline sfloat detector::mean_over_area(star_map * Image, int x, int y, int radius) {

    int counter = 0;
    int sum = 0;
    int x1, y1;
    // x1 and y1 are used to loop over the region to compute mean over area.
    for (x1 = x - radius; x1 <= x + radius; x1++) {
        if (x1 <= -Image->width / 2 || x1 >= Image->width / 2)
            continue;
        for (y1 = y - radius; y1 <= y + radius; y1++) {
            if (y1 <= -Image->height / 2 + 1 || y1 >= Image->height / 2 + 1)
                continue;
            sum += Image->get(x1, y1);
            counter += 1;
        }
    }

    return ( counter > 0 ? (sfloat) sum / counter : 0);
}

//It is useful to compute the mean of the middle half of several coordinates.
//This allows us to find the local background over an area, excluding pixels which
//may be stars. 

inline sfloat detector::mean_middle_half(vector<cart_coords>& ind) {
    if (ind.empty())
        return 0;

    sfloat middle_sum = 0; //mean of middle half of ind

    sort(ind.begin(), ind.end());

    vector<cart_coords>::iterator it;
    int first_quart = (int) (ind.size() * 0.25);
    int third_quart = (int) (ind.size() * 0.75);
    for (it = ind.begin() + first_quart; it != ind.begin() + third_quart; it++) {
        middle_sum += it->value;
    }

    return middle_sum / (third_quart - first_quart);
}


//finds median over annulus. Standard aperture photometry
//not sure if we want to use this. 

/*
inline sfloat detector::median_over_annulus(star_map * Image, int x, int y, int outter_radius, int inner_radius) {
    vector<int> pixels; //stores pixel values in annulus
    int x1, y1;
    // x1 and y1 are used to loop over the region to compute mean over area.
    for (x1 = x - outter_radius; x1 <= x + outter_radius; x1++) {
        if (x1 <= -Image->width / 2 || x1 >= Image->width / 2) //check x1 is inside range
            continue;
        for (y1 = y - outter_radius; y1 <= y + outter_radius; y1++) {
            if (y1 <= -Image->height / 2 + 1 || y1 >= Image->height / 2 + 1 //check y1 is inside range
                    || x1 * x1 + y1 * y1 < inner_radius * inner_radius //break if inside inner_radius
                    || x1 * x1 + y1 * y1 > outter_radius * outter_radius) //break if outisde outter_radius
                continue;
            pixels.push_back(Image->get(x1, y1));
        }
    }

    //compute median in next part

    int size = pixels.size();

    sort(pixels.begin(), pixels.end());
    if (size % 2 == 0)
        return (pixels[size / 2 - 1] + pixels[size / 2]) / 2;
    else
        return pixels[size / 2];

}*/

inline bool detector::isOutstanding(star_map * Image, int x, int y, double outst_val, double low_val) {
    if (Image->get(x, y) >= outst_val && mean_over_area(Image, x, y, MEAN_VALUE_RADIUS) >= low_val) {
        filteredMap->set(x, y, MAX_VAL);
    }
    return ( Image->get(x, y) >= outst_val);
}



//new recursive method. Passes vector of coordinates, which allows CentroidAt to do aperture photometry
//OLD:    detector_recurse( Image, marked, low_val, sample_min, x, y, &total_count, &outstnd_count, &x_numer, &y_numer, &sum );
//NEW:    detector_recurse( Image, low_val, outst_val, mean, &ind );

void detector::detector_recurse(star_map * Image, double low_val, double outst_val, double background_mean,
        int x, int y, vector<cart_coords>& ind, bool diag, sfloat D) {
    // x and y are coordinates of the pixel to be checked.
    if (x <= -Image->width / 2 || x >= Image->width / 2 || y <= -Image->height / 2 + 1 || y >= Image->height / 2 + 1)
        return;

    int outstnd_count = ind.size();

    //conditions to end. Don't check marked pixels, and terminate if too many pixels.
    if (is_marked(Image, x, y) || outstnd_count >= STAR_MAX_OUTSTND)
        return;

    //needed?
    //short pixel = Image->get(x, y);

    bool outstanding = isOutstanding(Image, x, y, outst_val, low_val);

    //Next, check if pixel is outstanding. If so, mark it and continue recursion.
    if (outstanding) {
        mark(Image, x, y);
        //cout << x << ", " << y << endl;
        cart_coords coords;
        coords.x = x;
        coords.y = y;
        coords.D = 0;
        coords.value = Image->get(x, y);
        ind.push_back(coords);

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) //
                    continue;
                detector_recurse(Image, low_val, outst_val, background_mean, x + i, y + j,
                        ind, (i + j) % 2 == 0, 0);
            }
        }
    }

    //Note sure if will use (too slow):
    //if the pixel is not outstanding, we still want to add it to the list (change third component)
    //when sufficiently close.

    //10 here is max number of pixels considered for photometry beyond marked pixels.
    //we can use these for either centroiding for finding a local background.
    /*else if (D < 10) { 
        //distance of new pixel from marked pixel. Depends on whether diagonal or not
        sfloat D_new = diag ? D + sqrt(2) : D + 1;
        //check to see if already added
        vector<cart_coords>::iterator it;
        bool match = false;
        for (it = ind.begin(); it != ind.end(); it++) {
            if (it->x == x && it->y == y)
                match = true;
                if (D_new < it->D ){
                    it->D = D_new;
                    continue;
                }
        }
        if (!match){
            cart_coords coords;
            coords.x = x;
            coords.y = y;
            coords.D = D_new;
            coords.value = Image->get(x, y);
            ind.push_back(coords);
        }

        if (D_new >= 10)
            return;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) //
                    continue;
                detector_recurse(Image, low_val, outst_val, background_mean, x + i, y + j,
                        ind, (i+j)%2 == 0, D_new) ;
            }
        }
    }*/
}

//old recursive method .
//detector_recurse( Image, marked, low_val, sample_min, x, y, &total_count, &outstnd_count, &x_numer, &y_numer, &sum );

void detector::detector_recurse(star_map * Image, double low_val, double outst_val, double mean,
        int x, int y, int* total_count, int* outstnd_count,
        long long* x_numer, long long* y_numer, long long* sum) {
    // x and y are defined the same way as below in the detectstars method
    if (x <= -Image->width / 2 || x >= Image->width / 2 || y <= -Image->height / 2 + 1 || y >= Image->height / 2 + 1)
        *outstnd_count = -1;

    //conditions to end
    if (*outstnd_count < 0 || is_marked(Image, x, y) || *outstnd_count >= STAR_MAX_OUTSTND)
        return;

    short pixel = Image->get(x, y);
    bool outstanding = isOutstanding(Image, x, y, outst_val, low_val);

    //Next, check if pixel is outstanding 
    if (outstanding || *outstnd_count >= 1) {
        mark(Image, x, y);
        int val = (int) pixel - mean;

        *x_numer += val * x;
        *y_numer += val * y;
        *sum += pixel;
        *total_count += 1;

        if (outstanding) {
            *outstnd_count += 1;
            //Image->set(x,y,4095);

            int i, j;
            for (i = -1; i <= 1; i++) {
                for (j = -1; j <= 1; j++) {
                    if (i == 0 && j == 0)
                        continue;
                    detector_recurse(Image, low_val, outst_val, mean, x + i, y + j,
                            total_count, outstnd_count, x_numer, y_numer, sum);
                }
            }
        }
    }
}


//new centroiding method. Based on aperture photometry
// ...(describe details)

inline void detector::CentroidAtAperture(star_map* Image, vector<image_star>& ImageStars,
        double low_val, double outst_val, double background_mean, int x, int y) {
    //new algorithm: store pixels in an array

    //each number describes the coordinates of the pixels
    // cart_coords is vector<pair<int,int>>, defined in detector.h
    vector<cart_coords> ind;

    //done inside of detector_recurse
    //ind.push_back((cart_coords) make_pair(x, y)); //add the initial pixel's own coordinates

    detector_recurse(Image, low_val, outst_val, background_mean, x, y, ind, false, 0);

    vector<cart_coords>::iterator it;

    //number of pixels in star
    int total_count = ind.size();
    if (total_count < STAR_MIN_OUTSTND)
        return;
    sfloat radius = sqrt((sfloat) total_count / (sfloat) M_PI);

    //sum of values of star pixels. Used for centroiding.
    /*sfloat sum = 0;
    for (it = ind.begin(); it != ind.end(); it++) {
        sum += Image->get(it->x, it->y);
    }*/
    //implemented later

    //approximate coordinates used to estimate centroid
    sfloat rough_X = 0;
    sfloat rough_Y = 0;
    sfloat rough_denom = 0;

    //number used to weigh pixel values when finding centroid
    // sfloat denom = sum - (total_count * background_mean);

    //cout << "sum: " << sum << ", " << "denom: " << denom << endl;


    image_star star;
    sfloat flat_value;
    //check conditions on pixels to help verify they make up a star
    //rough round, no aperture photometry. computed for comparison.
    for (it = ind.begin(); it != ind.end(); it++) {
        //not necessary if not using algorithm with D
        //if (it->D != 0.0) //for rough_X and rough_Y, we use only outstanding pixels
        //    continue;
        flat_value = it->value - background_mean;
        rough_X += ((sfloat) it->x) * flat_value;
        rough_Y += ((sfloat) it->y) * flat_value;
        rough_denom += flat_value;
    }
    rough_X /= rough_denom;
    rough_Y /= rough_denom;

    //find max distance we need to consider for aperture
    sfloat max_distance_sqr = 0;
    sfloat dist_sqr;
    for (it = ind.begin(); it != ind.end(); it++) {
        dist_sqr = (rough_X - it->x)*(rough_X - it->x) + (rough_Y - it->y)*(rough_Y - it->y);
        if (dist_sqr > max_distance_sqr)
            max_distance_sqr = dist_sqr;
    }
    // cout << "max_distance_sqr: " << max_distance_sqr << endl;
    //make the outer aperture twice the maximum star radius (can be changed later)
    sfloat outer_radius = 2.5 * sqrt(max_distance_sqr);

    //Next part uses aperture photometry.

    int X = (int) rough_X;
    int Y = (int) rough_Y;
    sfloat frac_X = rough_X - X;
    sfloat frac_Y = rough_Y - Y;
    //ind.clear();

    vector<cart_coords> out;
    sfloat rad_sqr;
    cart_coords coords;
    // x1 and y1 are used to loop.
    for (int x1 = X - outer_radius; x1 <= X + outer_radius; x1++) {
        if (x1 <= -Image->width / 2 || x1 >= Image->width / 2) //check x1 is inside range
            continue;
        for (int y1 = Y - outer_radius; y1 <= Y + outer_radius; y1++) {
            if (y1 <= -Image->height / 2 || y1 >= Image->height / 2) //check y1 is inside range
                continue;
            rad_sqr = (x1 + frac_X - X)*(x1 + frac_X - X)+(y1 + frac_Y - Y)*(y1 + frac_Y - Y);
            /*if (rad_sqr <= max_distance_sqr * 1.2) {
                coords.x = x1;
                coords.y = y1;
                coords.value = Image->get(x1,y1);
                ind.push_back(coords);
            }*/
            if (rad_sqr >= max_distance_sqr * 1.3 && rad_sqr <= 2.5 * max_distance_sqr) {
                coords.x = x1;
                coords.y = y1;
                coords.value = Image->get(x1, y1);
                out.push_back(coords);
            }
        }
    }

    sfloat aptr_X = 0;
    sfloat aptr_Y = 0;
    sfloat aptr_denom = 0;
   // cout << "out.size(): " << out.size() << endl;
    sfloat aperture_mean = mean_middle_half(out);
   // cout << "aperture_mean: " << aperture_mean << endl;

    //rough round, no aperture photometry. computed for comparison.
    for (it = ind.begin(); it != ind.end(); it++) {
        //not necessary if not using algorithm with D
        //if (it->D != 0.0) //for rough_X and rough_Y, we use only outstanding pixels
        //    continue;
        flat_value = it->value - aperture_mean;
        aptr_X += ((sfloat) it->x) * flat_value;
        aptr_Y += ((sfloat) it->y) * flat_value;
        aptr_denom += flat_value;
    }
    aptr_X /= aptr_denom;
    aptr_Y /= aptr_denom;

   // cout << "rough_X (with 0.5): " << rough_X + 0.5 << endl;
   // cout << "rough_Y (with 0.5): " << rough_Y + 0.5 << endl;

    cout << "aptr_X (with 0.5): " << aptr_X + 0.5 << endl;
    cout << "aptr_Y (with 0.5): " << aptr_Y + 0.5 << endl;

    //coordinate system issue
    star.centroid_x = 0.5 + aptr_X;
    star.centroid_y = 0.5 + aptr_Y;

    /*
    //first, compute the background values to use for photometry
    vector<cart_coords> out;
    for (it = ind.begin(); it != ind.end(); it++)
        if (it->D >= 6) //at least 6 pixels from outstanding pixels
            out.push_back(*it);
    
   aperture_mean = mean_middle_half(out);
   
   cout << "aperture_mean = " << aperture_mean << ", background_mean = " << background_mean 
           << ", number in outter apperture: " << out.size()
           << endl;
   
   for (it = ind.begin(); it != ind.end(); it++) {
        if (it->D != 0.0) //for rough_X and rough_Y, we use only outstanding pixels
            continue;
        flat_value = it->value - aperture_mean;
        aptr_X += ((sfloat) it->x) * flat_value ;
        aptr_Y += ((sfloat) it->y) * flat_value ;
        aptr_denom += flat_value;
    }
    aptr_X /= aptr_denom;
    aptr_Y /= aptr_denom;
     */




    //shift by 0.5 to deal with different coordinate system

    cout << "X: " << star.centroid_x << " Y: " << star.centroid_y << endl;


    star.identity = INDEX_INVALID;
    star.error = ERROR_MULTIPLIER * error_function(atan(radius / Image->FocalLength));
    star.radius = radius;

    // process image distortion, find angular distances
    // stores the (spherical vector) coordinates of the star with respect to the plate (GetSphericalFromImage).
    GetSphericalFromImage(star.centroid_x, star.centroid_y, 4293, star.r_prime);

    ImageStars.push_back(star);
    /*//used for photometry. Represents maximum radius
     sfloat max_radius_squared = 1;
     int D_sqrd = 0;
     for(int i=0; i < ind.size() - 1; i++){
         for (int j>i; j < ind.size(); j++){
             D_sqrd = ind[i]%width* ind[i]%width + ind[i]/width*ind[i]/width;
             if (max_radius_squared < ind[i]%width* ind[i]%width + ind[i]/width*ind[i]/width )
                     max_radius_squared = D_sqrd;
         }
     }
     */
}

//for a pixel that has not been marked, but has been found to be outstanding, 
//try to find a star to which that pixel belongs

inline void detector::CentroidAt(star_map* Image, vector<image_star>& ImageStars,
        double low_val, double outst_val, double mean, int x, int y) {

    //old algorithm: use numbers to store data about pixels
    int total_count = 0;
    int outstnd_count = 0;
    long long sum = 0;
    long long x_numer = 0;
    long long y_numer = 0;

    detector_recurse(Image, low_val, outst_val, mean,
            x, y, &total_count, &outstnd_count,
            &x_numer, &y_numer, &sum);

    unsigned long long denom = sum - (total_count * mean);

    // Area = pi*r^2
    sfloat radius = sqrt(outstnd_count / (sfloat) M_PI);

    if (denom > 0 && outstnd_count >= STAR_MIN_OUTSTND && radius <= MAX_RADIUS) {
        image_star star;
        //TODO: Test the centroid positions and make sure they are correct
        //Especially consider the 0.5 or 1.0 shifts.

        star.centroid_x = 0.5 + x_numer / (sfloat) denom;
        star.centroid_y = 0.5 + (y_numer / (sfloat) denom);

        cout << "X: " << star.centroid_x << " Y: " << star.centroid_y << endl;



        star.identity = INDEX_INVALID;
        star.error = ERROR_MULTIPLIER * error_function(atan(radius / Image->FocalLength));
        star.radius = radius;

        // process image distortion, find angular distances
        // stores the (spherical vector) coordinates of the star with respect to the plate (GetSphericalFromImage).
        GetSphericalFromImage(star.centroid_x, star.centroid_y, Image->FocalLength, star.r_prime);

        ImageStars.push_back(star);
    }
}

bool detector::compare_radius(image_star& left, image_star& right) {
    return (left.radius > right.radius);
}

size_t detector::DetectStars(vector<image_star>& ImageStars, star_map* Image) {
#ifdef USING_UNIX    
    time_t unixSeconds;
    time(&unixSeconds);
#endif
    unsigned int num_image_pixels = Image->width * Image->height;
    unsigned int num_gl_sample_pixels = (Image->width / gl_sample_skip - 1) * (Image->height / gl_sample_skip - 1);

    marked.reserve(num_image_pixels);
    for (int x = 0; x < num_image_pixels; x++)
        marked[x] = false;
    //is_marked(Image)

    //vector<image_star> ImageStars;
    ImageStars.clear();
    //ImageStars.reserve(500);

    vector<short> LocalSample;
    LocalSample.reserve((local_width / local_sample_skip - 1) * (local_height / local_sample_skip - 1));

    //sky_sum and sky_count are used to compute the average background values
    unsigned long long sky_sum = 0;
    unsigned int sky_count = 0;

    debug_printf("Detecting stars...\n");

    /*TEST*/
    for (int z = -CAMERA_WIDTH / 2; z < CAMERA_WIDTH / 2; z += local_width)
        for (int q = -CAMERA_HEIGHT / 2 + 1; q < CAMERA_HEIGHT / 2 + 1; q++) {
            filteredMap->set(z, q, MAX_VAL);
        }
    for (int q = -CAMERA_HEIGHT / 2 + 1; q < CAMERA_HEIGHT / 2; q += local_height)
        for (int z = -CAMERA_WIDTH / 2; z < CAMERA_WIDTH / 2; z++) {
            filteredMap->set(z, q, MAX_VAL);
        }
    /*TEST*/

    int i, j, x, y;
    // we break star map into a bunch of squares, and use j and i to iterate over them
    // j and i are the bottom left corner of the box. 
    for (j = -Image->height / 2 + 1; j < Image->height / 2 + 1; j += local_height) {
        for (i = -Image->width / 2; i < Image->width / 2; i += local_width) {
            LocalSample.clear();
            // x and y are used to iterate through the local box, defined by i and j
            for (y = j + local_sample_skip; y < j + local_height && y < Image->height / 2 + 1; y += local_sample_skip)
                for (x = i + local_sample_skip; x < i + local_width && x < Image->width / 2; x += local_sample_skip)
                    LocalSample.push_back(Image->get(x, y));

            //sort( LocalSample.begin(), LocalSample.end() );

            size_t N = LocalSample.size();
            /*short sample_min = LocalSample[0];
            short Q1 = LocalSample[ N/4 ];
            short median = LocalSample[ N/2 ];
            short Q3 = LocalSample[ N*3/4 ];*/

            //statistics for the local sample
            //These will be used to find "outstanding" pixels to identify stars.
            double mean = 0.0;
            double st_dev = 0;

            // compute the mean of the local sample
            for (unsigned int k = 0; k < N; k++)
                mean += LocalSample[k];
            mean = mean / (double) N;

            //compute the standard deviation of the local sample
            /*for (unsigned int k = 0; k < N; k++)
                st_dev += (LocalSample[k]-mean)*(LocalSample[k]-mean);
            st_dev /= (double) N;
            st_dev = sqrt(st_dev);*/
            st_dev = sqrt(mean);
            //cout<< "mean:" << mean << "  st_dev: "<< st_dev << endl;

            /*int low_val = (int)( Q3 + 1.0f * ( Q3 - Q1 ) );
            cout << "Q1\t" << Q1 << endl;
            cout << "Q3\t" << Q3 << endl;
            cout << "low_val\t" << low_val << endl;*/
            double outst_val = mean + OUTST_MULT * st_dev;
            double low_val = mean + LOW_VAL_MULT * st_dev;

#if 0
            printf("(%02d, %02d) min = %d, Q1 = %d, median = %d, Q3 = %d, max = %d, outliers at %d\n",
                    i, j, sample_min, Q1, median, Q3, LocalSample[ N - 1 ], low_val);
#endif

            // get star pixels and find centroid
            for (y = j; y < j + local_height && y < Image->height / 2 + 1; y++)
                for (x = i; x < i + local_width && x < Image->width / 2; x++) {

                    if (!isOutstanding(Image, x, y, outst_val, low_val)) {
                        //compute the average background values
                        sky_sum += Image->get(x, y);
                        sky_count += 1;
                    } else if (!is_marked(Image, x, y)) {
                        CentroidAtAperture(Image, ImageStars, low_val, outst_val, mean, x, y);
                    }
                    //if the pixel is not marked and the mean value of the nearby pixels is high enough,
                    //apply the CentroidAt method about the pixel, which marks the pixel and
                    //


                    /*	if ( !isOutstanding(Image, x, y, low_val) )
                        {
                                sky_sum += Image->get(x, y);
                                sky_count += 1;
                        }
                        else if ( !is_marked(Image, x, y) && mean_over_area(Image, x, y, 1) >= Q3 + outst_val )
                        {
                                CentroidAt(Image, ImageStars, low_val, sample_min, x, y);
                        }
                     */

                }
        }
    }

    size_t ret = ImageStars.size();
    /*TEST*/
    filteredMap->printTo_FIT_file("filteredBlack.fit");
    /*TEST*/

    /*#ifdef MAX_IMAGE_STARS
            vector<image_star>::iterator nth = ( ImageStars.size() > MAX_IMAGE_STARS ? ImageStars.begin() + MAX_IMAGE_STARS : ImageStars.end() );
            nth_element( ImageStars.begin(), nth, ImageStars.end(), compare_radius );
            ImageStars.erase(nth, ImageStars.end());
    #endif*/

    //using windows
    //timer.StopTimer();

    mean_sky = (sfloat) ((double) sky_sum / sky_count);

#ifdef DEBUG_TEXT
    vector<image_star>::itimeterator it;
    for (it = ImageStars.begin(); it != ImageStars.end(); it++) {
        printf("Image Star %03d: (%.3f, %.3f) | Error =  %.15f | r' = (%f,%f,%f)\n",
                int(it - ImageStars.begin()) + 1, it->centroid_x, it->centroid_y, it->error, it->r_prime[0], it->r_prime[1], it->r_prime[2]);
    }
#endif

    //need to re-define timer for this...
    //debug_printf("ImageStars successfully found, size: %d | Time elapsed: %d ms\n", ImageStars.size(), timer.GetTime());
#ifdef PROMPT_USER
    printf("Press ENTER to continue.\n");
    getchar();
#endif

    //return ImageStars;

    return ret;
}
