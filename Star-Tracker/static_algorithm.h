/* 
 * File:   static_algorithm.h
 * Author: Karan
 *
 * Created on January 13, 2013, 6:37 PM
 */

#include "catalog.h"
#include "star_map.h"
#include "detector.h"
#include "identifier.h"
#include "attitude.h"
#include "star_map.h"
#include "star.h"

class star_map;

#ifndef STATIC_ALGORITHM_H
#define	STATIC_ALGORITHM_H

class static_algorithm {
public:
    //populates the image stars in the star_map based on its catalog and the prior
    static_algorithm(){}; 
    void runAlgorithm(star_map* s);
private:

};

#endif	/* STATIC_ALGORITHM_H */

