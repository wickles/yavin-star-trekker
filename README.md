Star-Tracker
============
GJT KST
	Star Tracker project for Lubin 

This project is the version of the star tracker using classes.
The code has the ability to generate artificial sky images,
including stars from a catalog and noise (parameters can be
changed).

However, the code is UNTESTED. More work is required before 
the code can be used.

One possible bug is in the generator near the poles. 
We have experienced difficulties in finding solutions
in these regions. 

This code was created using NetBeans IDE on Linux and Mac, so it 
will have to be adjusted to compile and run using Windows Visual
Studio.